/*
 *
 *  MIT License
 *
 *  Copyright (c) 2017 Alibaba Group
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

package com.example.distributorapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.example.distributorapp.R;
import com.example.distributorapp.models.PackageListRes;

import java.util.ArrayList;

public class UltraPagerAdapter extends PagerAdapter {

    private Context mContext;
    private boolean isMultiScr;
    private ArrayList<PackageListRes.Data> dataArrayList;
    private int selectedID = 0;

    public UltraPagerAdapter(Context context, boolean isMultiScr, ArrayList<PackageListRes.Data> dataArrayList) {
        this.mContext = context;
        this.isMultiScr = isMultiScr;
        this.dataArrayList = dataArrayList;
    }

    /*public void setSelectedID(int selectedID) {
        this.selectedID = selectedID;
    }*/

    @Override
    public int getCount() {
        return dataArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /*public int getItemPosition(Object object) {
        return POSITION_NONE;
    }*/

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_package_list_ultra, container, false);
        TextView tvPackageName = (TextView) view.findViewById(R.id.tvPackageName);
        TextView tvPackageAmount = (TextView) view.findViewById(R.id.tvPackageAmount);
        TextView tvPackageDuration = (TextView) view.findViewById(R.id.tvPackageDuration);
        TextView tvPersons = (TextView) view.findViewById(R.id.tvPersons);
        tvPackageName.setText(dataArrayList.get(position).getName());
        tvPackageAmount.setText("$"+dataArrayList.get(position).getAmount());
        tvPackageDuration.setText(""+dataArrayList.get(position).getDuration()+" Days");

        if (dataArrayList.get(position).getNum_of_persons() > 0) {
            tvPersons.setText(""+dataArrayList.get(position).getNum_of_persons()+" Persons");
            tvPersons.setVisibility(View.VISIBLE);
        } else {
            tvPersons.setVisibility(View.GONE);
        }

        /*if (PackageListActivity.selectedID == position) {
            tvPackageDuration.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            tvPackageDuration.setBackgroundColor(mContext.getResources().getColor(R.color.dark_gray));
        }*/

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
