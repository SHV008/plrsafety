package com.example.distributorapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.distributorapp.R;
import com.example.distributorapp.activities.PaypalPaymentActivity;
import com.example.distributorapp.models.PackageListRes;

import java.util.ArrayList;

public class PackageListAdapter extends RecyclerView.Adapter<PackageListAdapter.ViewHolder> {

    private Context mContext;
    protected ItemListener mListener;
    private ArrayList<PackageListRes.Data> listMessages;

    public PackageListAdapter(Context context, ArrayList<PackageListRes.Data> listMessages, ItemListener itemListener) {

        this.listMessages = listMessages;
        this.mContext = context;
        mListener=itemListener;
    }

    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    public void setListItem(ArrayList<PackageListRes.Data> listMessages) {
        this.listMessages = listMessages;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_package_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {
        final PackageListRes.Data data = listMessages.get(position);

        Vholder.tvPackageName.setText(data.getName());
        Vholder.tvValidity.setText("Validity : "+data.getDuration()+" Days");
        Vholder.tvAmount.setText("$ "+data.getAmount());
        //Vholder.tvPackageName.setText(data.getName());

        Vholder.btnSubscribeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PaypalPaymentActivity.class);
                intent.putExtra("PackageName", data.getName());
                intent.putExtra("Amount", data.getAmount());
                intent.putExtra("PackageID", ""+data.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return listMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvPackageName, tvValidity, tvAmount;
        private Button btnSubscribeNow;
        ArrayList<PackageListRes.Data> item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            tvPackageName = (TextView) v.findViewById(R.id.tvPackageName);
            tvValidity = (TextView) v.findViewById(R.id.tvValidity);
            tvAmount = (TextView) v.findViewById(R.id.tvAmount);
            btnSubscribeNow = (Button) v.findViewById(R.id.btnSubscribeNow);
        }

        @Override
        public void onClick(View view) {
            /*if (mListener != null) {
                mListener.onItemClick(item);
            }*/
        }
    }

    public interface ItemListener {
        void onItemClick(ArrayList<PackageListRes.Data> item);
    }
}