package com.example.distributorapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.distributorapp.R;
import com.example.distributorapp.models.NotificationListRes;
import java.util.ArrayList;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

    private Context mContext;
    protected ItemListener mListener;
    private ArrayList<NotificationListRes> listMessages;

    public NotificationListAdapter(Context context, ArrayList<NotificationListRes> listMessages, ItemListener itemListener) {
        this.listMessages = listMessages;
        this.mContext = context;
        mListener=itemListener;
    }

    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    public void setListItem(ArrayList<NotificationListRes> listMessages) {
        this.listMessages = listMessages;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_notifications_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {
        final NotificationListRes messagesData = listMessages.get(position);

        if (messagesData != null) {
            Vholder.tvTitle.setText(messagesData.getNotificationName());
            Vholder.tvDescription.setText(messagesData.getNotificationDescription());
            Vholder.tvDate.setText(messagesData.getDate());
        }
    }

    @Override
    public int getItemCount() {
        return listMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvTitle, tvDescription, tvDate;
        ArrayList<NotificationListRes> item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvDescription = (TextView) v.findViewById(R.id.tvDescription);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(ArrayList<NotificationListRes> item);
    }
}