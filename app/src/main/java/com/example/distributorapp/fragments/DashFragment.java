package com.example.distributorapp.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.example.distributorapp.NetworkChangeReceiver;
import com.example.distributorapp.R;
import com.example.distributorapp.activities.ImmidiateAssistanceActivity;
import com.example.distributorapp.activities.SettingsActivity;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.GetUserStateRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.example.distributorapp.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class DashFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSettings)
    ImageView ivSettings;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvLatLong)
    TextView tvLatLong;
    @BindView(R.id.ivSend)
    Button ivSend;
    @BindView(R.id.llSend)
    LinearLayout llSend;
    @BindView(R.id.llSent)
    LinearLayout llSent;
    @BindView(R.id.pbSending)
    ProgressBar pbSending;
    @BindView(R.id.btnStop)
    Button btnStop;
    @BindView(R.id.tvCount)
    TextView tvCount;
    @BindView(R.id.llProgress)
    LinearLayout llProgress;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    private BroadcastReceiver mNetworkReceiver;

    private android.app.AlertDialog.Builder alert;
    private android.app.AlertDialog alertDialog;

    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static double currentLatitude = 0.0;
    public static double currentLongitude = 0.0;
    public static String strAddress = "";

    int pStatus = 0;
    private Handler handler = new Handler();

    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dash_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initialiseMap();
        callGetUserAPI();

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();

        Log.v("AccessToken : ", "" + PrefHelper.getString(Constants.ACCESS_TOKEN, ""));
    }

    public void initialiseMap() {
        mGoogleApiClient = new GoogleApiClient.Builder(dashActivity)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.clear();

        mMap.setMyLocationEnabled(true);
        LatLng latLong = new LatLng(currentLatitude, currentLongitude);
        mMap.addMarker(new MarkerOptions().position(latLong).title(getCompleteAddressString(currentLatitude, currentLongitude)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLong, 17));
        //mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
    }

    @Override
    public void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * If connected get lat and long
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(dashActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(dashActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            setLocation();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(dashActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        setLocation();
        //Toast.makeText(this, currentLatitude + " : " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    public void setLocation() {
        //Toast.makeText(dashActivity, "Latitude : "+currentLatitude, Toast.LENGTH_SHORT).show();
        tvAddress.setText(getCompleteAddressString(currentLatitude, currentLongitude));
        tvLatLong.setText("Lat : " + currentLatitude + "  Long : " + currentLongitude);
        strAddress = getCompleteAddressString(currentLatitude, currentLongitude);
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(dashActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                //Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                //Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current address", "Canont get Address!");
        }
        return strAdd;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        showExitDialog();
    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(dashActivity);
        builder.setTitle("Are you sure to want to exit from app?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dashActivity.finish();
                    }
                });

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    @OnClick({R.id.ivBack, R.id.ivSend, R.id.ivSettings,R.id.btnStop})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                break;
            case R.id.ivSettings:
                startActivity(new Intent(dashActivity, SettingsActivity.class));
                break;
            case R.id.ivSend:
                /*if (!PrefHelper.getBoolean(Constants.IS_CONFORM_SMS, false)) {
                    dialogSMSAlert();
                } else {
                    startActivity(new Intent(dashActivity, ImmidiateAssistanceActivity.class));
                }*/

                startActivity(new Intent(dashActivity, ImmidiateAssistanceActivity.class));

                break;
            case R.id.btnStop:
                llSend.setVisibility(View.VISIBLE);
                llSent.setVisibility(View.GONE);
                llProgress.setVisibility(View.GONE);
                pStatus = 10;
                handler.removeMessages(0);
                break;
        }
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            if (phoneNo.startsWith("0")) {
                phoneNo = phoneNo.substring(1);
            }
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            //Toast.makeText(dashActivity, "Message Sent",Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(dashActivity, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public void displaySendingView() {
        pStatus = 0;
        llSend.setVisibility(View.GONE);
        llSent.setVisibility(View.GONE);
        llProgress.setVisibility(View.VISIBLE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus < 5) {
                    pStatus += 1;
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            pbSending.setProgress(pStatus);
                            tvCount.setText((5 - pStatus) + "");
                            if (pStatus == 5) {
                                llSend.setVisibility(View.GONE);
                                llSent.setVisibility(View.VISIBLE);
                                llProgress.setVisibility(View.GONE);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        llSend.setVisibility(View.VISIBLE);
                                        llSent.setVisibility(View.GONE);
                                        llProgress.setVisibility(View.GONE);
                                        pStatus = 0;
                                    }
                                }, 3000);

                                if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                                    if (Utils.getInstance().checkInternetConnection(dashActivity)) {
                                        if (PrefHelper.getBoolean(Constants.IS_LOGIN, false)) {
                                            //if (PrefHelper.getBoolean(Constants.IS_SEND_MESSAGE, false)) {
                                            String senderNumber = PrefHelper.getString(Constants.COUNTRY_CODE, "") + PrefHelper.getString(Constants.PRIMARY_EMERGENCY_NUMBER, "");
                                            if (currentLatitude != 0.0 && currentLongitude != 0.0) {
                                                sendSMS("" + senderNumber, "IMMEDIATE ASSISTANCE REQUIRED : Address - " + getCompleteAddressString(currentLatitude, currentLongitude) + ", Lat : " + String.format("%.4f", currentLatitude) + "  Long : " + String.format("%.4f", currentLongitude) + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                                            } else {
                                                sendSMS("" + senderNumber, "IMMEDIATE ASSISTANCE REQUIRED : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                                            }
                                        }
                                    } else {
                                        Toast.makeText(dashActivity, "Network is not available so once network is available then it will be sent message automatically", Toast.LENGTH_LONG).show();
                                        String message = "";
                                        if (currentLatitude != 0.0 && currentLongitude != 0.0) {
                                            message = "IMMEDIATE ASSISTANCE REQUIRED : Address - " + getCompleteAddressString(currentLatitude, currentLongitude) + ", Lat : " + String.format("%.4f", currentLatitude) + "  Long : " + String.format("%.4f", currentLongitude) + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, "");
                                        } else {
                                            message = "IMMEDIATE ASSISTANCE REQUIRED : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, "");
                                        }

                                        PrefHelper.setString(Constants.STORED_MESSAGE, "" + message);
                                    }
                                } else {
                                    PaymentFragment bottomSheetFragment = new PaymentFragment();
                                    bottomSheetFragment.show(dashActivity.getSupportFragmentManager(), bottomSheetFragment.getTag());
                                }
                            }
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(1500); //thread will take approx 3 seconds to finish,change its value according to your needs
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            dashActivity.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dashActivity.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeMessages(0);
    }

    protected void unregisterNetworkChanges() {
        try {
            dashActivity.unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    private void callGetUserAPI() {
        Call<GetUserStateRes> objectCall = RestClient.getApiClient().getUserState();
        dashActivity.restClient.makeApiRequest(dashActivity, objectCall, this, Constants.REQ_CODE_GET_USER_STATE, false);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_GET_USER_STATE:
                GetUserStateRes getUserStateRes = (GetUserStateRes) response.body();
                PrefHelper.setBoolean(Constants.IS_PAID_USER, getUserStateRes.getData().isSubscription());
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_GET_MY_COURSES_LIST:
                Toast.makeText(dashActivity, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(dashActivity, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnClick(R.id.btnStop)
    public void onViewClicked() {
    }

    private void dialogSMSAlert() {
        LayoutInflater li = LayoutInflater.from(dashActivity);
        final View confirmDialog = li.inflate(R.layout.dialog_sms_alert, null);

        Button btnConfirm = (Button)confirmDialog.findViewById(R.id.btnConfirm);
        Button btnCancel = (Button)confirmDialog.findViewById(R.id.btnCancel);

        alert = new android.app.AlertDialog.Builder(dashActivity);
        alert.setView(confirmDialog);
        alert.setCancelable(true);
        alertDialog = alert.create();
        alertDialog.show();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PrefHelper.setBoolean(Constants.IS_CONFORM_SMS, true);
                startActivity(new Intent(dashActivity, ImmidiateAssistanceActivity.class));
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

}
