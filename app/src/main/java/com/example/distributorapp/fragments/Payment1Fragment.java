package com.example.distributorapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.distributorapp.R;
import com.example.distributorapp.activities.PackageListActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class Payment1Fragment extends BottomSheetDialogFragment {

    private Button btnDialogActivate;
    private View view;

    public Payment1Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.layout_bottom_sheet1, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnDialogActivate = view.findViewById(R.id.btnSubscribeNow);
        btnDialogActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PackageListActivity.class);
                startActivity(intent);
            }
        });
    }
}