package com.example.distributorapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.distributorapp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class ValidatePromoCodeFragment extends BottomSheetDialogFragment {

    private Button btnValidate;
    private EditText edtPromoCode;
    private View view;

    private String strPromoCode = "", PackageName = "", PackageID = "", Promocode = "";
    private int Amount = 0;

    public ValidatePromoCodeFragment() {
        // Required empty public constructor
    }

    public ValidatePromoCodeFragment(String Promocode, String PackageName, int Amount, String PackageID) {
        this.Promocode = Promocode;
        this.PackageName = PackageName;
        this.Amount = Amount;
        this.PackageID = PackageID;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.layout_promo_code, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edtPromoCode = view.findViewById(R.id.edtPromoCode);
        btnValidate = view.findViewById(R.id.btnValidate);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strPromoCode = edtPromoCode.getText().toString().trim();
                if (!TextUtils.isEmpty(strPromoCode)) {
                    /*Intent intent = new Intent(getActivity(), PaypalPaymentActivity.class);
                    intent.putExtra("PackageName", PackageName);
                    intent.putExtra("Amount", Amount);
                    intent.putExtra("PackageID", "" + PackageID);
                    startActivity(intent);*/
                    Intent intent1 = new Intent();
                    intent1.putExtra("PromoCode", strPromoCode);
                    intent1.setAction("CheckAndCreatePromoCode");
                    getActivity().sendBroadcast(intent1);
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please enter promo code", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (!TextUtils.isEmpty(Promocode)) {
            edtPromoCode.setText(Promocode);
        }
    }
}