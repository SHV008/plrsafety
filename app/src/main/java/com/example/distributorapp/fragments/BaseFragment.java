package com.example.distributorapp.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.distributorapp.IdealDistributorApp;
import com.example.distributorapp.PermissionResult;
import com.example.distributorapp.R;
import com.example.distributorapp.activities.MainActivity;
import com.example.distributorapp.apis.ApiResponseListener;
import com.example.distributorapp.interfaces.BackPressedEventListener;
import com.example.distributorapp.interfaces.ClickEventListener;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public abstract class BaseFragment extends Fragment implements BackPressedEventListener, ClickEventListener, ApiResponseListener {

    public MainActivity dashActivity;

    public static final String TAG = "BaseFragment";

    private int KEY_PERMISSION = 0;
    private PermissionResult permissionResult;
    private String permissionsAsk[];

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dashActivity = (MainActivity) getActivity();
    }

    // Arbitrary value; set it to some reasonable default
    private static final int DEFAULT_CHILD_ANIMATION_DURATION = 250;

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        final Fragment parent = getParentFragment();

        // Apply the workaround only if this is a child fragment, and the parent
        // is being removed.
        if (!enter && parent != null && parent.isRemoving()) {
             /*Note:This is a workaround for the bug where child fragments disappear when
                    the parent is removed (as all children are first removed from the parent)
                    See https://code.google.com/p/android/issues/detail?id=55228*/
            Animation doNothingAnim = new AlphaAnimation(1, 1);
            doNothingAnim.setDuration(getNextAnimationDuration(parent, DEFAULT_CHILD_ANIMATION_DURATION));
            return doNothingAnim;
        } else {

            if (IdealDistributorApp.DISABLE_FRAGMENT_ANIMATION) {
                if (!enter) {
                    Animation anim = null;
                    if (nextAnim != 0) {
//                        anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);
                        anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
                        //if(anim!=null)
                        anim.setDuration(0);
                        //return anim;
                    } else {
                        /*Note: If next animation is not there then it will blink layout after remove all fragments.
                        *       So to overcome this issue here load any animation and set duration 0*/
                        anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);
                        anim.setDuration(0);
                    }
                    return anim;
                }
            }
        }

        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    private static long getNextAnimationDuration(Fragment fragment, long defValue) {
        try {
            // Attempt to get the resource ID of the next animation that
            // will be applied to the given fragment.
            Field nextAnimField = Fragment.class.getDeclaredField("mNextAnim");
            nextAnimField.setAccessible(true);
            int nextAnimResource = nextAnimField.getInt(fragment);
            if (nextAnimResource > 0) {
                Animation nextAnim = AnimationUtils.loadAnimation(fragment.getActivity(), nextAnimResource);
                // ...and if it can be loaded, return that animation's duration
                return (nextAnim == null) ? defValue : nextAnim.getDuration();
            } else
                return defValue;
        } catch (NoSuchFieldException | IllegalAccessException | Resources.NotFoundException ex) {
            //Log.w("getNextAnimationDuration", "Unable to load next animation from parent.", ex);
            ex.printStackTrace();
            return defValue;
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void clickEvent(View v) {

    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {

    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode, int position) {

    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {

    }

    @Override
    public void onDataNotFound(Call<Object> call, Object object, int reqCode) {

    }

    @SuppressWarnings({"MissingPermission"})
    public boolean isPermissionGranted(Context context, String permission) {
        return (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) ||
                (ActivityCompat.checkSelfPermission(context, permission) ==
                        PackageManager.PERMISSION_GRANTED);
    }

    public boolean isPermissionsGranted(Context context, String permissions[]) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        boolean granted = true;
        for (String permission : permissions) {
            if (!(ActivityCompat.checkSelfPermission(
                    context, permission) == PackageManager.PERMISSION_GRANTED))
                granted = false;
        }

        return granted;
    }

    private void internalRequestPermission(String[] permissionAsk) {
        String arrayPermissionNotGranted[];
        ArrayList<String> permissionsNotGranted = new ArrayList<>();

        for (String aPermissionAsk : permissionAsk) {
            if (!isPermissionGranted(getActivity(), aPermissionAsk)) {
                permissionsNotGranted.add(aPermissionAsk);
            }
        }

        if (permissionsNotGranted.isEmpty()) {

            if (permissionResult != null)
                permissionResult.permissionGranted();

        } else {

            arrayPermissionNotGranted = new String[permissionsNotGranted.size()];
            arrayPermissionNotGranted = permissionsNotGranted.toArray(arrayPermissionNotGranted);
            ActivityCompat.requestPermissions(getActivity(),
                    arrayPermissionNotGranted, KEY_PERMISSION);
        }
    }

    public void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode != KEY_PERMISSION) {
            return;
        }

        List<String> permissionDenied = new LinkedList<>();
        boolean granted = true;

        for (int i = 0; i < grantResults.length; i++) {

            if (!(grantResults[i] == PackageManager.PERMISSION_GRANTED)) {
                granted = false;
                permissionDenied.add(permissions[i]);
            }
        }

        if (permissionResult != null) {
            if (granted) {
                permissionResult.permissionGranted();
            } else {
                for (String s : permissionDenied) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), s)) {
                        permissionResult.permissionForeverDenied();
                        return;
                    }
                }
                permissionResult.permissionDenied();
            }
        }
    }

    public void askCompactPermission(String permission, PermissionResult permissionResult) {
        KEY_PERMISSION = 200;
        permissionsAsk = new String[]{permission};
        this.permissionResult = permissionResult;
        internalRequestPermission(permissionsAsk);
    }

    public void askCompactPermissions(String permissions[], PermissionResult permissionResult) {
        KEY_PERMISSION = 200;
        permissionsAsk = permissions;
        this.permissionResult = permissionResult;
        internalRequestPermission(permissionsAsk);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
