package com.example.distributorapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.GetUserStateRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class PaymentSuccessActivity extends BaseActivity {


    @BindView(R.id.tvExpiredDate)
    TextView tvExpiredDate;
    @BindView(R.id.ivGoToHome)
    Button ivGoToHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);
        ButterKnife.bind(this);

        callGetUserAPI();
    }

    @OnClick(R.id.ivGoToHome)
    public void onViewClicked() {
        Intent intent = new Intent(PaymentSuccessActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void callGetUserAPI() {
        Call<GetUserStateRes> objectCall = RestClient.getApiClient().getUserState();
        restClient.makeApiRequest(PaymentSuccessActivity.this, objectCall, this, Constants.REQ_CODE_GET_USER_STATE, false);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_GET_USER_STATE:
                GetUserStateRes getUserStateRes = (GetUserStateRes) response.body();
                if (getUserStateRes.isSuccess()) {
                    PrefHelper.setBoolean(Constants.IS_PAID_USER, getUserStateRes.getData().isSubscription());
                    tvExpiredDate.setText("" + changeDateFormat("yyyy-MM-dd", "dd MMM, yyyy", getUserStateRes.getData().getExpiration()));
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_GET_MY_COURSES_LIST:
                Toast.makeText(PaymentSuccessActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(PaymentSuccessActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private String changeDateFormat(String currentFormat,String requiredFormat,String dateString){
        String result="";
        /*if (Strings.isNullOrEmpty(dateString)){
            return result;
        }*/
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.getDefault());
        Date date=null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }
}
