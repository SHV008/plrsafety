/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.example.distributorapp.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.distributorapp.R;
import com.example.distributorapp.adapters.PackageListAdapter;
import com.example.distributorapp.adapters.UltraPagerAdapter;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.fragments.ValidatePromoCodeFragment;
import com.example.distributorapp.models.PackageListRes;
import com.example.distributorapp.models.PostPaymentDataReq;
import com.example.distributorapp.models.PostPaymentDataRes;
import com.example.distributorapp.models.ValidatePromoCodeReq;
import com.example.distributorapp.models.ValidatePromoCodeRes;
import com.example.distributorapp.models.ValidatePromoCodeRes2;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.transformer.UltraScaleTransformer;

import java.util.ArrayList;
import java.util.Iterator;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;


public class PackageListActivity extends BaseActivity implements PackageListAdapter.ItemListener,
        AdapterView.OnItemSelectedListener {
    private static final String LOGTAG = "PackageListActivity";
    @BindView(R.id.btnSubscribeNow)
    Button btnSubscribeNow;
    private Button btnPromoCode;

    private UltraViewPager ultraViewPager;
    public PagerAdapter adapter;
    private UltraViewPager.Orientation gravity_indicator;

    private int selectedPosition = 0;
    public static int selectedID = 0;

    private AlertDialog.Builder alert;
    private AlertDialog alertDialog;

    private String strPromoCode = "";
    private int PackageID = 0;

    private RecyclerView rvPackages;
    private TextView tvNoPackages;
    private ImageView ivBack;
    public ToggleSwitch toggleSwitch;
    private ArrayList<PackageListRes.Data> listMessages = new ArrayList<>();
    private ArrayList<PackageListRes.Data> listIndividualPlans = new ArrayList<>();
    private ArrayList<PackageListRes.Data> listGroupPlans = new ArrayList<>();

    public PackageListActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_list);
        ButterKnife.bind(this);

        rvPackages = (RecyclerView) findViewById(R.id.rvPackages);
        tvNoPackages = findViewById(R.id.tvNoPackages);
        ivBack = findViewById(R.id.ivBack);
        btnPromoCode = findViewById(R.id.btnPromoCode);

        initialisePager();
        initialiseToggleSwitch();

        registerReceiver(receiver, new IntentFilter("CheckAndCreatePromoCode"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(PackageListActivity.this, LinearLayoutManager.VERTICAL, false);
        rvPackages.setLayoutManager(layoutManager);

        callGetMyCoursesListAPI();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ultraViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedPosition = position;
                /*UltraPagerAdapter categoryListAdapter = (UltraPagerAdapter) ultraViewPager.getAdapter();
                if (categoryListAdapter != null && categoryListAdapter.getCount() > 0)
                    categoryListAdapter.setSelectedID(position);*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogVerifyPromoCode();
            }
        });
    }

    public void initialiseToggleSwitch() {
        toggleSwitch = (ToggleSwitch) findViewById(R.id.tsMode);
        toggleSwitch.setCheckedTogglePosition(0);
        PrefHelper.setBoolean(Constants.IS_SELECTED_INDIVIDUAL_PLAN, true);

        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (position == 0) {
                    PrefHelper.setBoolean(Constants.IS_SELECTED_INDIVIDUAL_PLAN, true);
                    initialisePager();
                } else {
                    PrefHelper.setBoolean(Constants.IS_SELECTED_INDIVIDUAL_PLAN, false);
                    initialisePager();
                }
            }
        });
    }

    public void initialisePager() {
        ultraViewPager = (UltraViewPager) findViewById(R.id.ultra_viewpager);
        ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
        if (!PrefHelper.getBoolean(Constants.IS_SELECTED_INDIVIDUAL_PLAN, true)) {
            adapter = new UltraPagerAdapter(this, true, listGroupPlans);
        } else {
            adapter = new UltraPagerAdapter(this, true, listIndividualPlans);
        }
        ultraViewPager.setAdapter(adapter);
        ultraViewPager.setMultiScreen(0.6f);
        ultraViewPager.setItemRatio(1.0f);
        ultraViewPager.setRatio(2.0f);
        //ultraViewPager.setMaxHeight(1200);
        ultraViewPager.setAutoMeasureHeight(false);
        gravity_indicator = UltraViewPager.Orientation.HORIZONTAL;
        ultraViewPager.setPageTransformer(false, new UltraScaleTransformer());
    }

    @Override
    public void onItemClick(ArrayList<PackageListRes.Data> item) {
        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
    }

    private void callGetMyCoursesListAPI() {
        Call<PackageListRes> objectCall = RestClient.getApiClient().getPackageList();
        restClient.makeApiRequest(this, objectCall, this, Constants.REQ_CODE_GET_MY_COURSES_LIST, true);
    }

    private void callValidatePromoCodeAPI(String promocode) {
        ValidatePromoCodeReq validatePromoCodeReq = new ValidatePromoCodeReq();
        validatePromoCodeReq.setPromo_code(promocode);

        Call<ValidatePromoCodeRes2> objectCall = RestClient.getApiClient().checkAndCreatePromoCode(validatePromoCodeReq);
        restClient.makeApiRequest(PackageListActivity.this, objectCall, this, Constants.REQ_CODE_CHECK_CREATE_PROMO_CODE, true);
    }

    private void callVerifyPromoCodeAPI() {
        ValidatePromoCodeReq validatePromoCodeReq = new ValidatePromoCodeReq();
        validatePromoCodeReq.setPromo_code(strPromoCode);


        Call<ValidatePromoCodeRes> objectCall = RestClient.getApiClient().validatePromoCode(validatePromoCodeReq);
        restClient.makeApiRequest(PackageListActivity.this, objectCall, this, Constants.REQ_CODE_VALIDATE_PROMO_CODE, true);
    }

    private void callPostPaymentDetailAPI(int packageID) {
        PostPaymentDataReq postPaymentDataReq = new PostPaymentDataReq();
        postPaymentDataReq.setPackage_id(""+packageID);
        postPaymentDataReq.setPayment_id("");
        postPaymentDataReq.setPayment_amount("");
        postPaymentDataReq.setPayment_status("Approved");
        postPaymentDataReq.setPromo_code(strPromoCode);
        if (!TextUtils.isEmpty(strPromoCode)) {
            postPaymentDataReq.setPayment_type("offer");
        } else {
            postPaymentDataReq.setPayment_type("Payment");
        }

        Call<PostPaymentDataRes> objectCall = RestClient.getApiClient().postPaymentDetails(postPaymentDataReq);
        restClient.makeApiRequest(PackageListActivity.this, objectCall, this, Constants.REQ_CODE_POST_PAYMENT_DETAILS, false);
    }

    private void bindPackagesList() {
        if (listMessages != null && listMessages.size() > 0) {
            rvPackages.setVisibility(View.VISIBLE);
            PackageListAdapter categoryListAdapter = (PackageListAdapter) rvPackages.getAdapter();
            if (categoryListAdapter != null && categoryListAdapter.getItemCount() > 0)
                categoryListAdapter.setListItem(listMessages);
            else {
                categoryListAdapter = new PackageListAdapter(PackageListActivity.this, listMessages, this);
                rvPackages.setAdapter(categoryListAdapter);
            }
        }
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_GET_MY_COURSES_LIST:
                PackageListRes messagesListRes = (PackageListRes) response.body();
                if (messagesListRes.getData().size() > 0) {
                    ultraViewPager.setVisibility(View.VISIBLE);
                    tvNoPackages.setVisibility(View.GONE);
                    listMessages = new ArrayList<>(messagesListRes.getData());
                    listIndividualPlans = new ArrayList<>(messagesListRes.getData());
                    listGroupPlans = new ArrayList<>(messagesListRes.getData());
                    //listGroupPlans.removeIf(listGroupPlans.get(0).getType().equalsIgnoreCase(""));


                    Iterator<PackageListRes.Data> iterator = listIndividualPlans.iterator();
                    while (iterator.hasNext()){
                        PackageListRes.Data name = iterator.next();
                        String type = name.getType();
                        if(type.equalsIgnoreCase("Groups")){
                            iterator.remove();
                        }
                    }

                    Iterator<PackageListRes.Data> iterator1 = listGroupPlans.iterator();
                    while (iterator1.hasNext()){
                        PackageListRes.Data name = iterator1.next();
                        String type = name.getType();
                        if(type.equalsIgnoreCase("Individual")){
                            iterator1.remove();
                        }
                    }

                    if (listIndividualPlans.size() > 0) {
                        //bindPackagesList();
                        initialisePager();
                    } else {
                        ultraViewPager.setVisibility(View.GONE);
                        tvNoPackages.setVisibility(View.VISIBLE);
                        tvNoPackages.setText("No packages available");
                    }
                } else {
                    ultraViewPager.setVisibility(View.GONE);
                    tvNoPackages.setVisibility(View.VISIBLE);
                    tvNoPackages.setText("Packages not found");
                }
                break;

            case Constants.REQ_CODE_CHECK_CREATE_PROMO_CODE:
                ValidatePromoCodeRes2 validatePromoCodeRes = (ValidatePromoCodeRes2) response.body();
                if (validatePromoCodeRes != null) {
                    if (validatePromoCodeRes.isSuccess()) {
                        //int PackageID = validatePromoCodeRes.getData();
                        Intent intent = new Intent(PackageListActivity.this, PaypalPaymentActivity.class);
                        intent.putExtra("PackageName", listGroupPlans.get(selectedPosition).getName());
                        intent.putExtra("Amount", listGroupPlans.get(selectedPosition).getAmount());
                        intent.putExtra("PackageID", "" + listGroupPlans.get(selectedPosition).getId());
                        intent.putExtra("PromoCode", "" + strPromoCode);
                        startActivity(intent);
                    } else {
                        Toast.makeText(PackageListActivity.this, "Promo code is invalid", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PackageListActivity.this, validatePromoCodeRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constants.REQ_CODE_VALIDATE_PROMO_CODE:
                ValidatePromoCodeRes validatePromoCodeRes1 = (ValidatePromoCodeRes) response.body();
                if (validatePromoCodeRes1 != null) {
                    if (validatePromoCodeRes1.isSuccess()) {
                        alertDialog.cancel();
                        PackageID = validatePromoCodeRes1.getData();
                        callPostPaymentDetailAPI(PackageID);
                    } else {
                        Toast.makeText(PackageListActivity.this, validatePromoCodeRes1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PackageListActivity.this, validatePromoCodeRes1.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                PostPaymentDataRes postPaymentDataRes = (PostPaymentDataRes) response.body();
                if (postPaymentDataRes.isSuccess()) {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                    PrefHelper.setBoolean(Constants.IS_PAID_USER, true);
                    Intent intent = new Intent(PackageListActivity.this, PaymentSuccessActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_GET_MY_COURSES_LIST:
                ultraViewPager.setVisibility(View.GONE);
                tvNoPackages.setVisibility(View.VISIBLE);
                tvNoPackages.setText(object.toString());
                break;
            case Constants.REQ_CODE_CHECK_CREATE_PROMO_CODE:
                Toast.makeText(PackageListActivity.this, "Promo code is not valid", Toast.LENGTH_SHORT).show();
                ValidatePromoCodeFragment validatePromoCodeFragment = new ValidatePromoCodeFragment("", listGroupPlans.get(selectedPosition).getName(),
                        listGroupPlans.get(selectedPosition).getAmount(), ""+listGroupPlans.get(selectedPosition).getId());
                validatePromoCodeFragment.show(getSupportFragmentManager(), validatePromoCodeFragment.getTag());
                break;
            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                Toast.makeText(PackageListActivity.this, ""+object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(PackageListActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(receiver);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (ultraViewPager.getIndicator() == null) {
            ultraViewPager.initIndicator();
            ultraViewPager.getIndicator().setOrientation(gravity_indicator);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.btnSubscribeNow)
    public void onViewClicked() {
        if (PrefHelper.getBoolean(Constants.IS_SELECTED_INDIVIDUAL_PLAN, true)) {
            Intent intent = new Intent(PackageListActivity.this, PaypalPaymentActivity.class);
            intent.putExtra("PackageName", listIndividualPlans.get(selectedPosition).getName());
            intent.putExtra("Amount", listIndividualPlans.get(selectedPosition).getAmount());
            intent.putExtra("PackageID", "" + listIndividualPlans.get(selectedPosition).getId());
            intent.putExtra("PromoCode", "" + strPromoCode);
            startActivity(intent);
        } else {
            /*Intent intent = new Intent(PackageListActivity.this, PaypalPaymentActivity.class);
            intent.putExtra("PackageName", listGroupPlans.get(selectedPosition).getName());
            intent.putExtra("Amount", listGroupPlans.get(selectedPosition).getAmount());
            intent.putExtra("PackageID", "" + listGroupPlans.get(selectedPosition).getId());
            startActivity(intent);*/

            ValidatePromoCodeFragment validatePromoCodeFragment = new ValidatePromoCodeFragment("", listGroupPlans.get(selectedPosition).getName(),
                    listGroupPlans.get(selectedPosition).getAmount(), ""+listGroupPlans.get(selectedPosition).getId());
            validatePromoCodeFragment.show(getSupportFragmentManager(), validatePromoCodeFragment.getTag());
        }

    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("CheckAndCreatePromoCode")) {
                strPromoCode = intent.getExtras().getString("PromoCode");
                callValidatePromoCodeAPI(strPromoCode);
            }
        }
    };

    private void dialogVerifyPromoCode() {
        LayoutInflater li = LayoutInflater.from(this);
        final View confirmDialog = li.inflate(R.layout.dialog_enter_promo_code, null);

        final EditText edtPromoCode = (EditText) confirmDialog.findViewById(R.id.edtPromoCode);
        Button btnVerify = (Button)confirmDialog.findViewById(R.id.btnVerify);

        alert = new AlertDialog.Builder(this);
        alert.setView(confirmDialog);
        alert.setCancelable(true);
        alertDialog = alert.create();
        alertDialog.show();
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strPromoCode = edtPromoCode.getText().toString().trim();
                if (!TextUtils.isEmpty(strPromoCode)) {
                    callVerifyPromoCodeAPI();
                } else {
                    Toast.makeText(PackageListActivity.this, "Please enter promo code", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
