package com.example.distributorapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.PostPaymentDataReq;
import com.example.distributorapp.models.PostPaymentDataRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import retrofit2.Call;
import retrofit2.Response;

public class PaypalPaymentActivity extends BaseActivity {

    private static final String TAG = "PaypalPaymentActivity";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    // note that these credentials will differ between live & sandbox environments.
    //private static final String CONFIG_CLIENT_ID = "ARQ2B3DDY9GOwK2G35zRn6U8AcRP2E04l0Sy3D44fZCQ_VS2YkNOurgJK76dJhrlZG70-4b-kOu5YPk4";
    private static final String CONFIG_CLIENT_ID = "AYehspxNkGkIU7zw9jRZSeyQen_KMJVl_-dyq91_-sfpOdqagHjrJaXUiYUnnkfjYI3p4eep3ZImL5XU";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 888888;

    private String strPackageID = "",strPaymentID = "", strpaymentStatus = "",
    strPaymentType = "";

    private int Amount = 0;
    private String PackageName = "", PromoCode = "";

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payapl_payment);

        if (getIntent() != null) {
            strPackageID = getIntent().getExtras().getString("PackageID");
            PackageName = getIntent().getExtras().getString("PackageName");
            Amount = getIntent().getExtras().getInt("Amount");
            PromoCode = getIntent().getExtras().getString("PromoCode");
        }

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        onRedirectToPaypal();
    }

    public void onRedirectToPaypal() {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(PaypalPaymentActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
        //finish();
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(""+Amount), "USD", ""+PackageName,
                paymentIntent);
    }

    private void callPostPaymentDetailAPI() {
        PostPaymentDataReq postPaymentDataReq = new PostPaymentDataReq();
        postPaymentDataReq.setPackage_id(""+strPackageID);
        postPaymentDataReq.setPayment_id(""+strPaymentID);
        postPaymentDataReq.setPayment_amount(""+Amount);
        postPaymentDataReq.setPayment_status(""+strpaymentStatus);
        postPaymentDataReq.setPromo_code(PromoCode);
        postPaymentDataReq.setPayment_type(strPaymentType);
        /*if (!TextUtils.isEmpty(PromoCode)) {
            postPaymentDataReq.setPayment_type("offer");
        } else {
            postPaymentDataReq.setPayment_type("Payment");
        }*/

        Call<PostPaymentDataRes> objectCall = RestClient.getApiClient().postPaymentDetails(postPaymentDataReq);
        restClient.makeApiRequest(PaypalPaymentActivity.this, objectCall, this, Constants.REQ_CODE_POST_PAYMENT_DETAILS, false);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                PostPaymentDataRes postPaymentDataRes = (PostPaymentDataRes) response.body();
                if (postPaymentDataRes.isSuccess()) {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                    PrefHelper.setBoolean(Constants.IS_PAID_USER, true);
                    Intent intent = new Intent(PaypalPaymentActivity.this, PaymentSuccessActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                Toast.makeText(PaypalPaymentActivity.this, ""+object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(PaypalPaymentActivity.this, ""+object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == REQUEST_CODE_PAYMENT) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        JSONObject jObj = new JSONObject(paymentDetails);
                        Log.v("Payment ID : ",""+jObj.getJSONObject("response").get("id"));
                        Log.v("Payment State : ",""+jObj.getJSONObject("response").get("state"));

                        strPaymentID = ""+jObj.getJSONObject("response").get("id");
                        strpaymentStatus = ""+jObj.getJSONObject("response").get("state");
                        strPaymentType = "Paypal";

                        callPostPaymentDetailAPI();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
