package com.example.distributorapp.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatCheckBox;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.CountryListRes;
import com.example.distributorapp.models.PostPaymentDataReq;
import com.example.distributorapp.models.PostPaymentDataRes;
import com.example.distributorapp.models.SignUpReq;
import com.example.distributorapp.models.SignUpRes;
import com.example.distributorapp.models.ValidatePromoCodeReq;
import com.example.distributorapp.models.ValidatePromoCodeRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.example.distributorapp.utils.PrefrenceHelper;
import com.example.distributorapp.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {

    @BindView(R.id.edtCountryCode)
    EditText edtCountryCode;
    @BindView(R.id.edtLocalNumber)
    EditText edtLocalNumber;
    @BindView(R.id.edtSecondaryNumber)
    EditText edtSecondaryNumber;
    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.edtEmailAddress)
    EditText edtEmailAddress;
    @BindView(R.id.edtPersonalNumber)
    EditText edtPersonalNumber;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtConfirmPassword)
    EditText edtConfirmPassword;
    @BindView(R.id.edtPromoCode)
    EditText edtPromoCode;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.btnValidate)
    Button btnValidate;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.ckhRemember)
    AppCompatCheckBox ckhRemember;
    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;

    private String[] countryCode = {};
    private int selectedCountry = -1, CountryID = 0, PackageID = 0;
    private String strPromoCode = "";

    private CountryListRes countryListRes;

    String strCountryCode = "[{\"name\":\"Israel\",\"dial_code\":\"+972\",\"code\":\"IL\"},{\"name\":\"Afghanistan\",\"dial_code\":\"+93\",\"code\":\"AF\"},{\"name\":\"Albania\",\"dial_code\":\"+355\",\"code\":\"AL\"},{\"name\":\"Algeria\",\"dial_code\":\"+213\",\"code\":\"DZ\"},{\"name\":\"AmericanSamoa\",\"dial_code\":\"+1 684\",\"code\":\"AS\"},{\"name\":\"Andorra\",\"dial_code\":\"+376\",\"code\":\"AD\"},{\"name\":\"Angola\",\"dial_code\":\"+244\",\"code\":\"AO\"},{\"name\":\"Anguilla\",\"dial_code\":\"+1 264\",\"code\":\"AI\"},{\"name\":\"Antigua and Barbuda\",\"dial_code\":\"+1268\",\"code\":\"AG\"},{\"name\":\"Argentina\",\"dial_code\":\"+54\",\"code\":\"AR\"},{\"name\":\"Armenia\",\"dial_code\":\"+374\",\"code\":\"AM\"},{\"name\":\"Aruba\",\"dial_code\":\"+297\",\"code\":\"AW\"},{\"name\":\"Australia\",\"dial_code\":\"+61\",\"code\":\"AU\"},{\"name\":\"Austria\",\"dial_code\":\"+43\",\"code\":\"AT\"},{\"name\":\"Azerbaijan\",\"dial_code\":\"+994\",\"code\":\"AZ\"},{\"name\":\"Bahamas\",\"dial_code\":\"+1 242\",\"code\":\"BS\"},{\"name\":\"Bahrain\",\"dial_code\":\"+973\",\"code\":\"BH\"},{\"name\":\"Bangladesh\",\"dial_code\":\"+880\",\"code\":\"BD\"},{\"name\":\"Barbados\",\"dial_code\":\"+1 246\",\"code\":\"BB\"},{\"name\":\"Belarus\",\"dial_code\":\"+375\",\"code\":\"BY\"},{\"name\":\"Belgium\",\"dial_code\":\"+32\",\"code\":\"BE\"},{\"name\":\"Belize\",\"dial_code\":\"+501\",\"code\":\"BZ\"},{\"name\":\"Benin\",\"dial_code\":\"+229\",\"code\":\"BJ\"},{\"name\":\"Bermuda\",\"dial_code\":\"+1 441\",\"code\":\"BM\"},{\"name\":\"Bhutan\",\"dial_code\":\"+975\",\"code\":\"BT\"},{\"name\":\"Bosnia and Herzegovina\",\"dial_code\":\"+387\",\"code\":\"BA\"},{\"name\":\"Botswana\",\"dial_code\":\"+267\",\"code\":\"BW\"},{\"name\":\"Brazil\",\"dial_code\":\"+55\",\"code\":\"BR\"},{\"name\":\"British Indian Ocean Territory\",\"dial_code\":\"+246\",\"code\":\"IO\"},{\"name\":\"Bulgaria\",\"dial_code\":\"+359\",\"code\":\"BG\"},{\"name\":\"Burkina Faso\",\"dial_code\":\"+226\",\"code\":\"BF\"},{\"name\":\"Burundi\",\"dial_code\":\"+257\",\"code\":\"BI\"},{\"name\":\"Cambodia\",\"dial_code\":\"+855\",\"code\":\"KH\"},{\"name\":\"Cameroon\",\"dial_code\":\"+237\",\"code\":\"CM\"},{\"name\":\"Canada\",\"dial_code\":\"+1\",\"code\":\"CA\"},{\"name\":\"Cape Verde\",\"dial_code\":\"+238\",\"code\":\"CV\"},{\"name\":\"Cayman Islands\",\"dial_code\":\"+ 345\",\"code\":\"KY\"},{\"name\":\"Central African Republic\",\"dial_code\":\"+236\",\"code\":\"CF\"},{\"name\":\"Chad\",\"dial_code\":\"+235\",\"code\":\"TD\"},{\"name\":\"Chile\",\"dial_code\":\"+56\",\"code\":\"CL\"},{\"name\":\"China\",\"dial_code\":\"+86\",\"code\":\"CN\"},{\"name\":\"Christmas Island\",\"dial_code\":\"+61\",\"code\":\"CX\"},{\"name\":\"Colombia\",\"dial_code\":\"+57\",\"code\":\"CO\"},{\"name\":\"Comoros\",\"dial_code\":\"+269\",\"code\":\"KM\"},{\"name\":\"Congo\",\"dial_code\":\"+242\",\"code\":\"CG\"},{\"name\":\"Cook Islands\",\"dial_code\":\"+682\",\"code\":\"CK\"},{\"name\":\"Costa Rica\",\"dial_code\":\"+506\",\"code\":\"CR\"},{\"name\":\"Croatia\",\"dial_code\":\"+385\",\"code\":\"HR\"},{\"name\":\"Cuba\",\"dial_code\":\"+53\",\"code\":\"CU\"},{\"name\":\"Cyprus\",\"dial_code\":\"+537\",\"code\":\"CY\"},{\"name\":\"Czech Republic\",\"dial_code\":\"+420\",\"code\":\"CZ\"},{\"name\":\"Denmark\",\"dial_code\":\"+45\",\"code\":\"DK\"},{\"name\":\"Djibouti\",\"dial_code\":\"+253\",\"code\":\"DJ\"},{\"name\":\"Dominica\",\"dial_code\":\"+1 767\",\"code\":\"DM\"},{\"name\":\"Dominican Republic\",\"dial_code\":\"+1 849\",\"code\":\"DO\"},{\"name\":\"Ecuador\",\"dial_code\":\"+593\",\"code\":\"EC\"},{\"name\":\"Egypt\",\"dial_code\":\"+20\",\"code\":\"EG\"},{\"name\":\"El Salvador\",\"dial_code\":\"+503\",\"code\":\"SV\"},{\"name\":\"Equatorial Guinea\",\"dial_code\":\"+240\",\"code\":\"GQ\"},{\"name\":\"Eritrea\",\"dial_code\":\"+291\",\"code\":\"ER\"},{\"name\":\"Estonia\",\"dial_code\":\"+372\",\"code\":\"EE\"},{\"name\":\"Ethiopia\",\"dial_code\":\"+251\",\"code\":\"ET\"},{\"name\":\"Faroe Islands\",\"dial_code\":\"+298\",\"code\":\"FO\"},{\"name\":\"Fiji\",\"dial_code\":\"+679\",\"code\":\"FJ\"},{\"name\":\"Finland\",\"dial_code\":\"+358\",\"code\":\"FI\"},{\"name\":\"France\",\"dial_code\":\"+33\",\"code\":\"FR\"},{\"name\":\"French Guiana\",\"dial_code\":\"+594\",\"code\":\"GF\"},{\"name\":\"French Polynesia\",\"dial_code\":\"+689\",\"code\":\"PF\"},{\"name\":\"Gabon\",\"dial_code\":\"+241\",\"code\":\"GA\"},{\"name\":\"Gambia\",\"dial_code\":\"+220\",\"code\":\"GM\"},{\"name\":\"Georgia\",\"dial_code\":\"+995\",\"code\":\"GE\"},{\"name\":\"Germany\",\"dial_code\":\"+49\",\"code\":\"DE\"},{\"name\":\"Ghana\",\"dial_code\":\"+233\",\"code\":\"GH\"},{\"name\":\"Gibraltar\",\"dial_code\":\"+350\",\"code\":\"GI\"},{\"name\":\"Greece\",\"dial_code\":\"+30\",\"code\":\"GR\"},{\"name\":\"Greenland\",\"dial_code\":\"+299\",\"code\":\"GL\"},{\"name\":\"Grenada\",\"dial_code\":\"+1 473\",\"code\":\"GD\"},{\"name\":\"Guadeloupe\",\"dial_code\":\"+590\",\"code\":\"GP\"},{\"name\":\"Guam\",\"dial_code\":\"+1 671\",\"code\":\"GU\"},{\"name\":\"Guatemala\",\"dial_code\":\"+502\",\"code\":\"GT\"},{\"name\":\"Guinea\",\"dial_code\":\"+224\",\"code\":\"GN\"},{\"name\":\"Guinea-Bissau\",\"dial_code\":\"+245\",\"code\":\"GW\"},{\"name\":\"Guyana\",\"dial_code\":\"+595\",\"code\":\"GY\"},{\"name\":\"Haiti\",\"dial_code\":\"+509\",\"code\":\"HT\"},{\"name\":\"Honduras\",\"dial_code\":\"+504\",\"code\":\"HN\"},{\"name\":\"Hungary\",\"dial_code\":\"+36\",\"code\":\"HU\"},{\"name\":\"Iceland\",\"dial_code\":\"+354\",\"code\":\"IS\"},{\"name\":\"India\",\"dial_code\":\"+91\",\"code\":\"IN\"},{\"name\":\"Indonesia\",\"dial_code\":\"+62\",\"code\":\"ID\"},{\"name\":\"Iraq\",\"dial_code\":\"+964\",\"code\":\"IQ\"},{\"name\":\"Ireland\",\"dial_code\":\"+353\",\"code\":\"IE\"},{\"name\":\"Israel\",\"dial_code\":\"+972\",\"code\":\"IL\"},{\"name\":\"Italy\",\"dial_code\":\"+39\",\"code\":\"IT\"},{\"name\":\"Jamaica\",\"dial_code\":\"+1 876\",\"code\":\"JM\"},{\"name\":\"Japan\",\"dial_code\":\"+81\",\"code\":\"JP\"},{\"name\":\"Jordan\",\"dial_code\":\"+962\",\"code\":\"JO\"},{\"name\":\"Kazakhstan\",\"dial_code\":\"+7 7\",\"code\":\"KZ\"},{\"name\":\"Kenya\",\"dial_code\":\"+254\",\"code\":\"KE\"},{\"name\":\"Kiribati\",\"dial_code\":\"+686\",\"code\":\"KI\"},{\"name\":\"Kuwait\",\"dial_code\":\"+965\",\"code\":\"KW\"},{\"name\":\"Kyrgyzstan\",\"dial_code\":\"+996\",\"code\":\"KG\"},{\"name\":\"Latvia\",\"dial_code\":\"+371\",\"code\":\"LV\"},{\"name\":\"Lebanon\",\"dial_code\":\"+961\",\"code\":\"LB\"},{\"name\":\"Lesotho\",\"dial_code\":\"+266\",\"code\":\"LS\"},{\"name\":\"Liberia\",\"dial_code\":\"+231\",\"code\":\"LR\"},{\"name\":\"Liechtenstein\",\"dial_code\":\"+423\",\"code\":\"LI\"},{\"name\":\"Lithuania\",\"dial_code\":\"+370\",\"code\":\"LT\"},{\"name\":\"Luxembourg\",\"dial_code\":\"+352\",\"code\":\"LU\"},{\"name\":\"Madagascar\",\"dial_code\":\"+261\",\"code\":\"MG\"},{\"name\":\"Malawi\",\"dial_code\":\"+265\",\"code\":\"MW\"},{\"name\":\"Malaysia\",\"dial_code\":\"+60\",\"code\":\"MY\"},{\"name\":\"Maldives\",\"dial_code\":\"+960\",\"code\":\"MV\"},{\"name\":\"Mali\",\"dial_code\":\"+223\",\"code\":\"ML\"},{\"name\":\"Malta\",\"dial_code\":\"+356\",\"code\":\"MT\"},{\"name\":\"Marshall Islands\",\"dial_code\":\"+692\",\"code\":\"MH\"},{\"name\":\"Martinique\",\"dial_code\":\"+596\",\"code\":\"MQ\"},{\"name\":\"Mauritania\",\"dial_code\":\"+222\",\"code\":\"MR\"},{\"name\":\"Mauritius\",\"dial_code\":\"+230\",\"code\":\"MU\"},{\"name\":\"Mayotte\",\"dial_code\":\"+262\",\"code\":\"YT\"},{\"name\":\"Mexico\",\"dial_code\":\"+52\",\"code\":\"MX\"},{\"name\":\"Monaco\",\"dial_code\":\"+377\",\"code\":\"MC\"},{\"name\":\"Mongolia\",\"dial_code\":\"+976\",\"code\":\"MN\"},{\"name\":\"Montenegro\",\"dial_code\":\"+382\",\"code\":\"ME\"},{\"name\":\"Montserrat\",\"dial_code\":\"+1664\",\"code\":\"MS\"},{\"name\":\"Morocco\",\"dial_code\":\"+212\",\"code\":\"MA\"},{\"name\":\"Myanmar\",\"dial_code\":\"+95\",\"code\":\"MM\"},{\"name\":\"Namibia\",\"dial_code\":\"+264\",\"code\":\"NA\"},{\"name\":\"Nauru\",\"dial_code\":\"+674\",\"code\":\"NR\"},{\"name\":\"Nepal\",\"dial_code\":\"+977\",\"code\":\"NP\"},{\"name\":\"Netherlands\",\"dial_code\":\"+31\",\"code\":\"NL\"},{\"name\":\"Netherlands Antilles\",\"dial_code\":\"+599\",\"code\":\"AN\"},{\"name\":\"New Caledonia\",\"dial_code\":\"+687\",\"code\":\"NC\"},{\"name\":\"New Zealand\",\"dial_code\":\"+64\",\"code\":\"NZ\"},{\"name\":\"Nicaragua\",\"dial_code\":\"+505\",\"code\":\"NI\"},{\"name\":\"Niger\",\"dial_code\":\"+227\",\"code\":\"NE\"},{\"name\":\"Nigeria\",\"dial_code\":\"+234\",\"code\":\"NG\"},{\"name\":\"Niue\",\"dial_code\":\"+683\",\"code\":\"NU\"},{\"name\":\"Norfolk Island\",\"dial_code\":\"+672\",\"code\":\"NF\"},{\"name\":\"Northern Mariana Islands\",\"dial_code\":\"+1 670\",\"code\":\"MP\"},{\"name\":\"Norway\",\"dial_code\":\"+47\",\"code\":\"NO\"},{\"name\":\"Oman\",\"dial_code\":\"+968\",\"code\":\"OM\"},{\"name\":\"Pakistan\",\"dial_code\":\"+92\",\"code\":\"PK\"},{\"name\":\"Palau\",\"dial_code\":\"+680\",\"code\":\"PW\"},{\"name\":\"Panama\",\"dial_code\":\"+507\",\"code\":\"PA\"},{\"name\":\"Papua New Guinea\",\"dial_code\":\"+675\",\"code\":\"PG\"},{\"name\":\"Paraguay\",\"dial_code\":\"+595\",\"code\":\"PY\"},{\"name\":\"Peru\",\"dial_code\":\"+51\",\"code\":\"PE\"},{\"name\":\"Philippines\",\"dial_code\":\"+63\",\"code\":\"PH\"},{\"name\":\"Poland\",\"dial_code\":\"+48\",\"code\":\"PL\"},{\"name\":\"Portugal\",\"dial_code\":\"+351\",\"code\":\"PT\"},{\"name\":\"Puerto Rico\",\"dial_code\":\"+1 939\",\"code\":\"PR\"},{\"name\":\"Qatar\",\"dial_code\":\"+974\",\"code\":\"QA\"},{\"name\":\"Romania\",\"dial_code\":\"+40\",\"code\":\"RO\"},{\"name\":\"Rwanda\",\"dial_code\":\"+250\",\"code\":\"RW\"},{\"name\":\"Samoa\",\"dial_code\":\"+685\",\"code\":\"WS\"},{\"name\":\"San Marino\",\"dial_code\":\"+378\",\"code\":\"SM\"},{\"name\":\"Saudi Arabia\",\"dial_code\":\"+966\",\"code\":\"SA\"},{\"name\":\"Senegal\",\"dial_code\":\"+221\",\"code\":\"SN\"},{\"name\":\"Serbia\",\"dial_code\":\"+381\",\"code\":\"RS\"},{\"name\":\"Seychelles\",\"dial_code\":\"+248\",\"code\":\"SC\"},{\"name\":\"Sierra Leone\",\"dial_code\":\"+232\",\"code\":\"SL\"},{\"name\":\"Singapore\",\"dial_code\":\"+65\",\"code\":\"SG\"},{\"name\":\"Slovakia\",\"dial_code\":\"+421\",\"code\":\"SK\"},{\"name\":\"Slovenia\",\"dial_code\":\"+386\",\"code\":\"SI\"},{\"name\":\"Solomon Islands\",\"dial_code\":\"+677\",\"code\":\"SB\"},{\"name\":\"South Africa\",\"dial_code\":\"+27\",\"code\":\"ZA\"},{\"name\":\"South Georgia and the South Sandwich Islands\",\"dial_code\":\"+500\",\"code\":\"GS\"},{\"name\":\"Spain\",\"dial_code\":\"+34\",\"code\":\"ES\"},{\"name\":\"Sri Lanka\",\"dial_code\":\"+94\",\"code\":\"LK\"},{\"name\":\"Sudan\",\"dial_code\":\"+249\",\"code\":\"SD\"},{\"name\":\"Suriname\",\"dial_code\":\"+597\",\"code\":\"SR\"},{\"name\":\"Swaziland\",\"dial_code\":\"+268\",\"code\":\"SZ\"},{\"name\":\"Sweden\",\"dial_code\":\"+46\",\"code\":\"SE\"},{\"name\":\"Switzerland\",\"dial_code\":\"+41\",\"code\":\"CH\"},{\"name\":\"Tajikistan\",\"dial_code\":\"+992\",\"code\":\"TJ\"},{\"name\":\"Thailand\",\"dial_code\":\"+66\",\"code\":\"TH\"},{\"name\":\"Togo\",\"dial_code\":\"+228\",\"code\":\"TG\"},{\"name\":\"Tokelau\",\"dial_code\":\"+690\",\"code\":\"TK\"},{\"name\":\"Tonga\",\"dial_code\":\"+676\",\"code\":\"TO\"},{\"name\":\"Trinidad and Tobago\",\"dial_code\":\"+1 868\",\"code\":\"TT\"},{\"name\":\"Tunisia\",\"dial_code\":\"+216\",\"code\":\"TN\"},{\"name\":\"Turkey\",\"dial_code\":\"+90\",\"code\":\"TR\"},{\"name\":\"Turkmenistan\",\"dial_code\":\"+993\",\"code\":\"TM\"},{\"name\":\"Turks and Caicos Islands\",\"dial_code\":\"+1 649\",\"code\":\"TC\"},{\"name\":\"Tuvalu\",\"dial_code\":\"+688\",\"code\":\"TV\"},{\"name\":\"Uganda\",\"dial_code\":\"+256\",\"code\":\"UG\"},{\"name\":\"Ukraine\",\"dial_code\":\"+380\",\"code\":\"UA\"},{\"name\":\"United Arab Emirates\",\"dial_code\":\"+971\",\"code\":\"AE\"},{\"name\":\"United Kingdom\",\"dial_code\":\"+44\",\"code\":\"GB\"},{\"name\":\"United States\",\"dial_code\":\"+1\",\"code\":\"US\"},{\"name\":\"Uruguay\",\"dial_code\":\"+598\",\"code\":\"UY\"},{\"name\":\"Uzbekistan\",\"dial_code\":\"+998\",\"code\":\"UZ\"},{\"name\":\"Vanuatu\",\"dial_code\":\"+678\",\"code\":\"VU\"},{\"name\":\"Wallis and Futuna\",\"dial_code\":\"+681\",\"code\":\"WF\"},{\"name\":\"Yemen\",\"dial_code\":\"+967\",\"code\":\"YE\"},{\"name\":\"Zambia\",\"dial_code\":\"+260\",\"code\":\"ZM\"},{\"name\":\"Zimbabwe\",\"dial_code\":\"+263\",\"code\":\"ZW\"},{\"name\":\"land Islands\",\"dial_code\":\"\",\"code\":\"AX\"},{\"name\":\"Antarctica\",\"dial_code\":null,\"code\":\"AQ\"},{\"name\":\"Bolivia, Plurinational State of\",\"dial_code\":\"+591\",\"code\":\"BO\"},{\"name\":\"Brunei Darussalam\",\"dial_code\":\"+673\",\"code\":\"BN\"},{\"name\":\"Cocos (Keeling) Islands\",\"dial_code\":\"+61\",\"code\":\"CC\"},{\"name\":\"Congo, The Democratic Republic of the\",\"dial_code\":\"+243\",\"code\":\"CD\"},{\"name\":\"Cote d'Ivoire\",\"dial_code\":\"+225\",\"code\":\"CI\"},{\"name\":\"Falkland Islands (Malvinas)\",\"dial_code\":\"+500\",\"code\":\"FK\"},{\"name\":\"Guernsey\",\"dial_code\":\"+44\",\"code\":\"GG\"},{\"name\":\"Holy See (Vatican City State)\",\"dial_code\":\"+379\",\"code\":\"VA\"},{\"name\":\"Hong Kong\",\"dial_code\":\"+852\",\"code\":\"HK\"},{\"name\":\"Iran, Islamic Republic of\",\"dial_code\":\"+98\",\"code\":\"IR\"},{\"name\":\"Isle of Man\",\"dial_code\":\"+44\",\"code\":\"IM\"},{\"name\":\"Jersey\",\"dial_code\":\"+44\",\"code\":\"JE\"},{\"name\":\"Korea, Democratic People's Republic of\",\"dial_code\":\"+850\",\"code\":\"KP\"},{\"name\":\"Korea, Republic of\",\"dial_code\":\"+82\",\"code\":\"KR\"},{\"name\":\"Lao People's Democratic Republic\",\"dial_code\":\"+856\",\"code\":\"LA\"},{\"name\":\"Libyan Arab Jamahiriya\",\"dial_code\":\"+218\",\"code\":\"LY\"},{\"name\":\"Macao\",\"dial_code\":\"+853\",\"code\":\"MO\"},{\"name\":\"Macedonia, The Former Yugoslav Republic of\",\"dial_code\":\"+389\",\"code\":\"MK\"},{\"name\":\"Micronesia, Federated States of\",\"dial_code\":\"+691\",\"code\":\"FM\"},{\"name\":\"Moldova, Republic of\",\"dial_code\":\"+373\",\"code\":\"MD\"},{\"name\":\"Mozambique\",\"dial_code\":\"+258\",\"code\":\"MZ\"},{\"name\":\"Palestinian Territory, Occupied\",\"dial_code\":\"+970\",\"code\":\"PS\"},{\"name\":\"Pitcairn\",\"dial_code\":\"+872\",\"code\":\"PN\"},{\"name\":\"Réunion\",\"dial_code\":\"+262\",\"code\":\"RE\"},{\"name\":\"Russia\",\"dial_code\":\"+7\",\"code\":\"RU\"},{\"name\":\"Saint Barthélemy\",\"dial_code\":\"+590\",\"code\":\"BL\"},{\"name\":\"Saint Helena, Ascension and Tristan Da Cunha\",\"dial_code\":\"+290\",\"code\":\"SH\"},{\"name\":\"Saint Kitts and Nevis\",\"dial_code\":\"+1 869\",\"code\":\"KN\"},{\"name\":\"Saint Lucia\",\"dial_code\":\"+1 758\",\"code\":\"LC\"},{\"name\":\"Saint Martin\",\"dial_code\":\"+590\",\"code\":\"MF\"},{\"name\":\"Saint Pierre and Miquelon\",\"dial_code\":\"+508\",\"code\":\"PM\"},{\"name\":\"Saint Vincent and the Grenadines\",\"dial_code\":\"+1 784\",\"code\":\"VC\"},{\"name\":\"Sao Tome and Principe\",\"dial_code\":\"+239\",\"code\":\"ST\"},{\"name\":\"Somalia\",\"dial_code\":\"+252\",\"code\":\"SO\"},{\"name\":\"Svalbard and Jan Mayen\",\"dial_code\":\"+47\",\"code\":\"SJ\"},{\"name\":\"Syrian Arab Republic\",\"dial_code\":\"+963\",\"code\":\"SY\"},{\"name\":\"Taiwan, Province of China\",\"dial_code\":\"+886\",\"code\":\"TW\"},{\"name\":\"Tanzania, United Republic of\",\"dial_code\":\"+255\",\"code\":\"TZ\"},{\"name\":\"Timor-Leste\",\"dial_code\":\"+670\",\"code\":\"TL\"},{\"name\":\"Venezuela, Bolivarian Republic of\",\"dial_code\":\"+58\",\"code\":\"VE\"},{\"name\":\"Viet Nam\",\"dial_code\":\"+84\",\"code\":\"VN\"},{\"name\":\"Virgin Islands, British\",\"dial_code\":\"+1 284\",\"code\":\"VG\"},{\"name\":\"Virgin Islands, U.S.\",\"dial_code\":\"+1 340\",\"code\":\"VI\"}]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        String tkn = FirebaseInstanceId.getInstance().getToken();
        callGetCountryListAPI();
    }

    @OnClick({R.id.btnRegister, R.id.tv_sign_up, R.id.edtCountryCode, R.id.btnValidate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (isValidate()) {
                    callSignUpAPI();
                }
                break;
            case R.id.tv_sign_up:
                startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                break;

            case R.id.btnValidate:
                String PromoCode = edtPromoCode.getText().toString().trim();
                if (!TextUtils.isEmpty(PromoCode)) {
                    callValidatePromoCodeAPI(PromoCode);
                } else {
                    edtPromoCode.requestFocus();
                    Toast.makeText(this, "Please enter promo code", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.edtCountryCode:
                getCountryList();
                break;
        }
    }

    public boolean isValidate() {
        boolean isValid = true;
        String strCountryCode = edtCountryCode.getText().toString().trim();
        String strPrimaryEmergencyNumber = edtLocalNumber.getText().toString().trim();
        String strSecondaryEmergencyNumber = edtSecondaryNumber.getText().toString().trim();
        String strName = edtFirstName.getText().toString().trim();
        String strMobileNumber = edtPersonalNumber.getText().toString().trim();
        String strEmail = edtEmailAddress.getText().toString().trim();
        String strPassword = edtPassword.getText().toString().trim();
        String strConfirmPassword = edtConfirmPassword.getText().toString().trim();
        if (selectedCountry == -1) {
            isValid = false;
            Toast.makeText(this, "Please enter Country Code", Toast.LENGTH_SHORT).show();
        } else if (strPrimaryEmergencyNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter local emergency contact", Toast.LENGTH_SHORT).show();
        } /*else if (strSecondaryEmergencyNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter secondary emergency number", Toast.LENGTH_SHORT).show();
        }*/ else if (strName.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Name", Toast.LENGTH_SHORT).show();
        } else if (strMobileNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter personal Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (strEmail.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
        } else if (!Utils.isValidEmaillId(strEmail)) {
            isValid = false;
            Toast.makeText(this, "Please enter valid Email", Toast.LENGTH_SHORT).show();
        } else if (strPassword.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Password", Toast.LENGTH_SHORT).show();
        } else if (strConfirmPassword.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
        } else if (!strPassword.equalsIgnoreCase(strConfirmPassword)) {
            isValid = false;
            Toast.makeText(this, "Password and Confirm Password must be same", Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }

    private void callSignUpAPI() {
        SignUpReq signUpReq = new SignUpReq();
        signUpReq.setEmail(edtEmailAddress.getText().toString().trim());
        signUpReq.setPassword(edtPassword.getText().toString().trim());
        signUpReq.setAddress(edtAddress.getText().toString().trim());
        signUpReq.setDevice_token(PrefrenceHelper.getDeviceToken(SignUpActivity.this));
        signUpReq.setEmergency_primary_contact_number(edtLocalNumber.getText().toString().trim());
        signUpReq.setEmergency_secondary_contact_number(edtSecondaryNumber.getText().toString().trim());
        signUpReq.setName(edtFirstName.getText().toString().trim());
        signUpReq.setPassword_confirmation(edtConfirmPassword.getText().toString().trim());
        signUpReq.setPhone_number(edtPersonalNumber.getText().toString().trim());
        signUpReq.setPromo_code(edtPromoCode.getText().toString().trim());
        signUpReq.setCountry_id(""+CountryID);

        Call<SignUpRes> objectCall = RestClient.getApiClient().register(signUpReq);
        restClient.makeApiRequest(SignUpActivity.this, objectCall, this, Constants.REQ_CODE_SIGN_UP, true);
    }

    private void callValidatePromoCodeAPI(String promocode) {
        ValidatePromoCodeReq validatePromoCodeReq = new ValidatePromoCodeReq();
        validatePromoCodeReq.setPromo_code(promocode);


        Call<ValidatePromoCodeRes> objectCall = RestClient.getApiClient().validatePromoCode(validatePromoCodeReq);
        restClient.makeApiRequest(SignUpActivity.this, objectCall, this, Constants.REQ_CODE_VALIDATE_PROMO_CODE, true);
    }

    private void callGetCountryListAPI() {
        Call<CountryListRes> objectCall = RestClient.getApiClient().getCountryList();
        restClient.makeApiRequest(SignUpActivity.this, objectCall, this, Constants.REQ_CODE_GET_COUNTRY_LIST, true);
    }

    private void callPostPaymentDetailAPI(int packageID) {
        PostPaymentDataReq postPaymentDataReq = new PostPaymentDataReq();
        postPaymentDataReq.setPackage_id(""+packageID);
        postPaymentDataReq.setPayment_id("");
        postPaymentDataReq.setPayment_amount("");
        postPaymentDataReq.setPayment_status("Approved");
        postPaymentDataReq.setPromo_code(strPromoCode);
        if (!TextUtils.isEmpty(strPromoCode)) {
            postPaymentDataReq.setPayment_type("offer");
        } else {
            postPaymentDataReq.setPayment_type("Payment");
        }

        Call<PostPaymentDataRes> objectCall = RestClient.getApiClient().postPaymentDetails(postPaymentDataReq);
        restClient.makeApiRequest(SignUpActivity.this, objectCall, this, Constants.REQ_CODE_POST_PAYMENT_DETAILS, false);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_SIGN_UP:
                SignUpRes loginRes = (SignUpRes) response.body();
                if (loginRes != null) {
                    if (loginRes.isSuccess()) {
                        PrefHelper.setBoolean(Constants.IS_LOGIN, true);
                        PrefHelper.setString(Constants.ACCESS_TOKEN, loginRes.getData().getToken());
                        PrefHelper.setString(Constants.PRIMARY_EMERGENCY_NUMBER, edtLocalNumber.getText().toString().trim());
                        PrefHelper.setString(Constants.SECONDARY_EMERGENCY_NUMBER, edtSecondaryNumber.getText().toString().trim());
                        PrefHelper.setString(Constants.FULL_NAME, edtFirstName.getText().toString().trim());
                        PrefHelper.setString(Constants.PERSONAL_MOBILE_NUMBER, edtPersonalNumber.getText().toString().trim());
                        PrefHelper.setString(Constants.EMAIL, edtEmailAddress.getText().toString().trim());
                        PrefHelper.setString(Constants.PASSWORD, edtPassword.getText().toString().trim());
                        Toast.makeText(SignUpActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                        if (TextUtils.isEmpty(strPromoCode)) {
                            startActivity(new Intent(this, TripPlannerActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        } else {
                            callPostPaymentDetailAPI(PackageID);
                        }
                    } else {
                        Iterator<String> keys = null;
                        try {
                            keys = new JSONObject(loginRes.getData().toString()).keys();
                            // get some_name_i_wont_know in str_Name
                            String str_Name=keys.next();
                            // get the value i care about
                            String value = new JSONObject(loginRes.getData().toString()).optString(str_Name);
                            Toast.makeText(SignUpActivity.this, "" + value, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } else {
                    Toast.makeText(SignUpActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constants.REQ_CODE_GET_COUNTRY_LIST:
                countryListRes = (CountryListRes) response.body();
                if (countryListRes != null) {
                    if (countryListRes.isSuccess()) {
                        //countryListResMain = countryListRes.getData();
                        //getCountryList();
                    } else {
                        Toast.makeText(SignUpActivity.this, "" + countryListRes.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignUpActivity.this, countryListRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constants.REQ_CODE_VALIDATE_PROMO_CODE:
                ValidatePromoCodeRes validatePromoCodeRes = (ValidatePromoCodeRes) response.body();
                if (validatePromoCodeRes != null) {
                    tvStatus.setVisibility(View.VISIBLE);
                    if (validatePromoCodeRes.isSuccess()) {
                        strPromoCode = edtPromoCode.getText().toString().trim();
                        tvStatus.setText("Promo code is valid");
                        tvStatus.setTextColor(getResources().getColor(R.color.green));
                        PackageID = validatePromoCodeRes.getData();
                    } else {
                        tvStatus.setText("Promo code is not valid");
                    }
                } else {
                    Toast.makeText(SignUpActivity.this, validatePromoCodeRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                PostPaymentDataRes postPaymentDataRes = (PostPaymentDataRes) response.body();
                if (postPaymentDataRes.isSuccess()) {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                    PrefHelper.setBoolean(Constants.IS_PAID_USER, true);
                    /*Intent intent = new Intent(SignUpActivity.this, PaymentSuccessActivity.class);
                    startActivity(intent);
                    finish();*/

                    startActivity(new Intent(this, TripPlannerActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                } else {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_SIGN_UP:
                //Toast.makeText(SignUpActivity.this, object.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(SignUpActivity.this, "Email is already registered", Toast.LENGTH_SHORT).show();
                break;
            case Constants.REQ_CODE_VALIDATE_PROMO_CODE:
                tvStatus.setVisibility(View.VISIBLE);
                tvStatus.setText("Promo code is not valid");
                tvStatus.setTextColor(getResources().getColor(R.color.red));
                break;
            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                Toast.makeText(SignUpActivity.this, ""+object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(SignUpActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void getCountryList() {
        try {
            final CharSequence[] countryList = new CharSequence[countryListRes.getData().size()];
            for (int i=0; i<countryListRes.getData().size(); i++) {
                countryList[i] = countryListRes.getData().get(i).getCountry_name() +" ("+countryListRes.getData().get(i).getCountry_dial_code()+")";
            }

            AlertDialog.Builder alert = new AlertDialog.Builder(SignUpActivity.this);
            alert.setTitle("Select Country");
            alert.setSingleChoiceItems(countryList, selectedCountry, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectedCountry = which;
                }
            });
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        edtCountryCode.setText(countryList[selectedCountry]);
                        CountryID = countryListRes.getData().get(selectedCountry).getId();
                        PrefHelper.setString(Constants.COUNTRY_CODE, countryListRes.getData().get(selectedCountry).getCountry_dial_code());
                        edtLocalNumber.requestFocus();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
