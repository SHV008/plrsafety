package com.example.distributorapp.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.GetUserStateRes;
import com.example.distributorapp.models.PostPaymentDataReq;
import com.example.distributorapp.models.PostPaymentDataRes;
import com.example.distributorapp.models.ValidatePromoCodeReq;
import com.example.distributorapp.models.ValidatePromoCodeRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class ActivatedPackagesActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvExpired)
    TextView tvExpired;
    @BindView(R.id.btnSubscribe)
    Button btnSubscribe;
    @BindView(R.id.ivBack1)
    ImageView ivBack1;
    @BindView(R.id.btnPromoCode)
    Button btnPromoCode;

    private AlertDialog.Builder alert;
    private AlertDialog alertDialog;

    private String strPromoCode = "";
    private int PackageID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscribed_packages);
        ButterKnife.bind(this);

        callGetUserAPI();
    }


    private void callGetUserAPI() {
        Call<GetUserStateRes> objectCall = RestClient.getApiClient().getUserState();
        restClient.makeApiRequest(ActivatedPackagesActivity.this, objectCall, this, Constants.REQ_CODE_GET_USER_STATE, true);
    }

    private void callVerifyPromoCodeAPI() {
        ValidatePromoCodeReq validatePromoCodeReq = new ValidatePromoCodeReq();
        validatePromoCodeReq.setPromo_code(strPromoCode);


        Call<ValidatePromoCodeRes> objectCall = RestClient.getApiClient().validatePromoCode(validatePromoCodeReq);
        restClient.makeApiRequest(ActivatedPackagesActivity.this, objectCall, this, Constants.REQ_CODE_VALIDATE_PROMO_CODE, true);
    }

    private void callPostPaymentDetailAPI(int packageID) {
        PostPaymentDataReq postPaymentDataReq = new PostPaymentDataReq();
        postPaymentDataReq.setPackage_id(""+packageID);
        postPaymentDataReq.setPayment_id("");
        postPaymentDataReq.setPayment_amount("");
        postPaymentDataReq.setPayment_status("Approved");
        postPaymentDataReq.setPromo_code(strPromoCode);
        if (!TextUtils.isEmpty(strPromoCode)) {
            postPaymentDataReq.setPayment_type("offer");
        } else {
            postPaymentDataReq.setPayment_type("Payment");
        }

        Call<PostPaymentDataRes> objectCall = RestClient.getApiClient().postPaymentDetails(postPaymentDataReq);
        restClient.makeApiRequest(ActivatedPackagesActivity.this, objectCall, this, Constants.REQ_CODE_POST_PAYMENT_DETAILS, false);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_GET_USER_STATE:
                GetUserStateRes getUserStateRes = (GetUserStateRes) response.body();
                if (getUserStateRes.isSuccess()) {
                    PrefHelper.setBoolean(Constants.IS_PAID_USER, getUserStateRes.getData().isSubscription());
                    btnSubscribe.setVisibility(View.GONE);
                    tvExpired.setText("Your membership will expired at " + changeDateFormat("yyyy-MM-dd", "dd MMM, yyyy", getUserStateRes.getData().getExpiration()));
                } else {
                    btnSubscribe.setVisibility(View.VISIBLE);
                    tvExpired.setText("You have not subscribed any packages");
                }
                break;

            case Constants.REQ_CODE_VALIDATE_PROMO_CODE:
                ValidatePromoCodeRes validatePromoCodeRes1 = (ValidatePromoCodeRes) response.body();
                if (validatePromoCodeRes1 != null) {
                    if (validatePromoCodeRes1.isSuccess()) {
                        alertDialog.cancel();
                        PackageID = validatePromoCodeRes1.getData();
                        callPostPaymentDetailAPI(PackageID);
                    } else {
                        Toast.makeText(ActivatedPackagesActivity.this, validatePromoCodeRes1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivatedPackagesActivity.this, validatePromoCodeRes1.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                PostPaymentDataRes postPaymentDataRes = (PostPaymentDataRes) response.body();
                if (postPaymentDataRes.isSuccess()) {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                    PrefHelper.setBoolean(Constants.IS_PAID_USER, true);
                    Intent intent = new Intent(ActivatedPackagesActivity.this, PaymentSuccessActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, ""+postPaymentDataRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_GET_MY_COURSES_LIST:
                Toast.makeText(ActivatedPackagesActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
            case Constants.REQ_CODE_POST_PAYMENT_DETAILS:
                Toast.makeText(ActivatedPackagesActivity.this, ""+object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(ActivatedPackagesActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private String changeDateFormat(String currentFormat, String requiredFormat, String dateString) {
        String result = "";
        /*if (Strings.isNullOrEmpty(dateString)){
            return result;
        }*/
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.getDefault());
        Date date = null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }

    @OnClick({R.id.ivBack, R.id.btnSubscribe, R.id.btnPromoCode})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.btnSubscribe:
                startActivity(new Intent(ActivatedPackagesActivity.this, PackageListActivity.class));
                finish();
                break;

            case R.id.btnPromoCode:
                dialogVerifyPromoCode();
                break;
        }
    }

    private void dialogVerifyPromoCode() {
        LayoutInflater li = LayoutInflater.from(this);
        final View confirmDialog = li.inflate(R.layout.dialog_enter_promo_code, null);

        final EditText edtPromoCode = (EditText) confirmDialog.findViewById(R.id.edtPromoCode);
        Button btnVerify = (Button)confirmDialog.findViewById(R.id.btnVerify);

        alert = new AlertDialog.Builder(this);
        alert.setView(confirmDialog);
        alert.setCancelable(true);
        alertDialog = alert.create();
        alertDialog.show();
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strPromoCode = edtPromoCode.getText().toString().trim();
                if (!TextUtils.isEmpty(strPromoCode)) {
                    callVerifyPromoCodeAPI();
                } else {
                    Toast.makeText(ActivatedPackagesActivity.this, "Please enter promo code", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
