package com.example.distributorapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.distributorapp.R;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.suke.widget.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSettings)
    ImageView ivSettings;
    @BindView(R.id.tvSendMessageText)
    TextView tvSendMessageText;
    @BindView(R.id.switch_button)
    SwitchButton switchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        initialiseViews();
    }

    public void initialiseViews() {
        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (isChecked) {
                    PrefHelper.setBoolean(Constants.IS_SEND_MESSAGE, true);
                    //Toast.makeText(SettingsActivity.this, "ON", Toast.LENGTH_SHORT).show();
                } else {
                    PrefHelper.setBoolean(Constants.IS_SEND_MESSAGE, false);
                    //Toast.makeText(SettingsActivity.this, "OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (PrefHelper.getBoolean(Constants.IS_SEND_MESSAGE,false)) {
            switchButton.setChecked(true);
        } else {
            switchButton.setChecked(false);
        }
    }

    @OnClick({R.id.ivBack, R.id.switch_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.switch_button:
                break;
        }
    }
}
