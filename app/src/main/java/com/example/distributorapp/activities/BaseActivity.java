package com.example.distributorapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.distributorapp.IdealDistributorApp;
import com.example.distributorapp.apis.ApiResponseListener;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.interfaces.BackPressedEventListener;
import com.example.distributorapp.interfaces.ClickEventListener;

import retrofit2.Call;
import retrofit2.Response;


public class BaseActivity extends AppCompatActivity implements BackPressedEventListener, ClickEventListener, ApiResponseListener {
    private static final String TAG = "BaseActivity";
    private Toast toast;

    public RestClient restClient;
    public IdealDistributorApp appInstance;

    protected SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appInstance = (IdealDistributorApp) getApplication();
        restClient = appInstance.getRestClient();
        //Fabric.with(this, new Crashlytics());
    }

    public IdealDistributorApp getApp() {
        return (IdealDistributorApp) this.getApplication();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void clickEvent(View v) {

    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {

    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode, int position) {

    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {

    }

    @Override
    public void onDataNotFound(Call<Object> call, Object object, int reqCode) {

    }
}
