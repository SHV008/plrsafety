package com.example.distributorapp.activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.distributorapp.R;
import com.example.distributorapp.utils.PermissionUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GuidelineActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
        PermissionUtils.PermissionResultCallback {

    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.btnUserSetup)
    Button btnUserSetup;
    @BindView(R.id.ivSwipe)
    ImageView ivSwipe;
    @BindView(R.id.llTripPlanner)
    LinearLayout llTripPlanner;
    @BindView(R.id.llRegister)
    LinearLayout llRegister;

    private boolean isPermissionGranted = false;
    private ArrayList<String> permissions = new ArrayList<>();
    private PermissionUtils permissionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guideline);
        ButterKnife.bind(this);

        permissionUtils = new PermissionUtils(GuidelineActivity.this);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissions.add(Manifest.permission.SEND_SMS);
        permissionUtils.check_permission(permissions, "Need  permission", 1);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(GuidelineActivity.this, SOSService.class));
        } else {
            startService(new Intent(GuidelineActivity.this, SOSService.class));
        }*/

        //startService(new Intent(GuidelineActivity.this, LockService.class));
        /*Intent backgroundService = new Intent(getApplicationContext(), ScreenOnOffBackgroundService.class);
        startService(backgroundService);*/
    }

    @OnClick({R.id.llTripPlanner, R.id.llRegister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llTripPlanner:
                startActivity(new Intent(GuidelineActivity.this, TripPlannerActivity.class));
                finish();
                break;
            case R.id.llRegister:
                startActivity(new Intent(GuidelineActivity.this, SignUpActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION", "GRANTED");
        isPermissionGranted = true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY", "GRANTED");
//        isPermissionGranted = true;
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION", "DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION", "NEVER ASK AGAIN");
    }

    //boolean doubleBackToExitPressedOnce = false;
    int clickTimes = 1;
    @OnClick(R.id.ivSwipe)
    public void onViewClicked() {
        /*if (clickTimes >= 3) {
            Toast.makeText(this, "3 Times Click for send user info", Toast.LENGTH_SHORT).show();
        } else {
            clickTimes++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    clickTimes = 1;
                }
            }, 2000);
        }*/
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }
}
