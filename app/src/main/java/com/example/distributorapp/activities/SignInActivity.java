package com.example.distributorapp.activities;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.app.ActivityCompat;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.LoginReq;
import com.example.distributorapp.models.LoginRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PermissionUtils;
import com.example.distributorapp.utils.PrefHelper;
import com.example.distributorapp.utils.PrefrenceHelper;
import com.example.distributorapp.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class SignInActivity extends BaseActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
        PermissionUtils.PermissionResultCallback {

    @BindView(R.id.edtUsername)
    protected EditText edtUsername;
    @BindView(R.id.edtPassword)
    protected EditText edtPassword;
    @BindView(R.id.ckhShow)
    AppCompatCheckBox ckhShow;
    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;
    @BindView(R.id.btn_sign_in)
    Button btnSignIn;
    @BindView(R.id.ckhRemember)
    AppCompatCheckBox ckhRemember;
    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;

    private boolean isPermissionGranted = false;
    private ArrayList<String> permissions = new ArrayList<>();
    private PermissionUtils permissionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.getGradient(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        permissionUtils = new PermissionUtils(SignInActivity.this);

        permissions.add(Manifest.permission.CALL_PHONE);
        permissionUtils.check_permission(permissions, "Need  permission", 1);

        String tkn = FirebaseInstanceId.getInstance().getToken();

        ckhShow = findViewById(R.id.ckhShow);
        ckhShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    // hide password
                    edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }

    @OnClick(R.id.tv_forgot_password)
    void onClickSignUpForgotPassord() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plrsafety.com/password/reset"));
        startActivity(browserIntent);
    }

    @OnClick({R.id.btn_sign_in})
    void onClickSignIn(Button button) {
        switch (button.getId()) {
            case R.id.btn_sign_in:
                if (validateModel()) {
                    callLoginAPI();
                    //startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                }
                break;
        }
    }

    private boolean validateModel() {
        String strUserName = edtUsername.getText().toString().trim();
        String strPassword = edtPassword.getText().toString().trim();

        if (TextUtils.isEmpty(strUserName)) {
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Utils.isValidEmaillId(strUserName)) {
            Toast.makeText(this, "Please enter valid Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(strPassword)) {
            Toast.makeText(this, "Please enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callLoginAPI() {
        LoginReq modelRequestLogin = new LoginReq();
        modelRequestLogin.setEmail(edtUsername.getText().toString().trim());
        modelRequestLogin.setPassword(edtPassword.getText().toString().trim());
        modelRequestLogin.setDevice_token(PrefrenceHelper.getDeviceToken(SignInActivity.this));

        Call<LoginRes> objectCall = RestClient.getApiClient().login(modelRequestLogin);
        restClient.makeApiRequest(SignInActivity.this, objectCall, this, Constants.REQ_CODE_LOGIN, true);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_LOGIN:
                LoginRes loginRes = (LoginRes) response.body();
                if (loginRes != null) {
                    if (loginRes.isSuccess()) {
                        PrefHelper.setBoolean(Constants.IS_LOGIN, true);
                        PrefHelper.setString(Constants.ACCESS_TOKEN, loginRes.getData().getAccess_token());
                        PrefHelper.setString(Constants.PRIMARY_EMERGENCY_NUMBER, loginRes.getData().getEmergency_primary_contact_number());
                        PrefHelper.setString(Constants.SECONDARY_EMERGENCY_NUMBER, loginRes.getData().getEmergency_secondary_contact_number());
                        PrefHelper.setString(Constants.FULL_NAME, loginRes.getData().getName());
                        PrefHelper.setString(Constants.PASSWORD, edtPassword.getText().toString().trim());
                        PrefHelper.setString(Constants.PERSONAL_MOBILE_NUMBER, loginRes.getData().getPhone_number());
                        PrefHelper.setString(Constants.COUNTRY_CODE, loginRes.getData().getCountry_code());
                        PrefHelper.setString(Constants.EMAIL, edtUsername.getText().toString().trim());
                        PrefHelper.setString(Constants.AREA, loginRes.getData().getGeneral_area_adventure());
                        PrefHelper.setString(Constants.ETD, loginRes.getData().getEtd());
                        PrefHelper.setString(Constants.ETA, loginRes.getData().getEta());
                        PrefHelper.setString(Constants.PARTY_MEMBER, loginRes.getData().getParty_name());
                        PrefHelper.setString(Constants.PARTY_NUMBER, loginRes.getData().getParty_members());
                        PrefHelper.setString(Constants.MEDICAL_CONDITION, loginRes.getData().getExisting_medical_condition());
                        Toast.makeText(SignInActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                        /*if (PrefHelper.getBoolean(Constants.IS_ADDED_TRIP_PLANNER,false)) {
                            startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            startActivity(new Intent(this, TripPlannerActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }*/
                        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();

                    } else {
                        Toast.makeText(SignInActivity.this, "" + loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignInActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_LOGIN:
                Toast.makeText(SignInActivity.this, object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(SignInActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION", "GRANTED");
        isPermissionGranted = true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY", "GRANTED");
//        isPermissionGranted = true;
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION", "DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION", "NEVER ASK AGAIN");
    }

    @OnClick(R.id.tv_sign_up)
    public void onViewClicked() {
        startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
    }
}
