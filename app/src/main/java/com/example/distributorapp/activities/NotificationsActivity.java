package com.example.distributorapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.distributorapp.R;
import com.example.distributorapp.adapters.NotificationListAdapter;
import com.example.distributorapp.models.NotificationListRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationsActivity extends BaseActivity implements NotificationListAdapter.ItemListener{

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivBack1)
    ImageView ivBack1;
    @BindView(R.id.tvNoPackages)
    TextView tvNoPackages;
    @BindView(R.id.rvPackages)
    RecyclerView rvPackages;

    private ArrayList<NotificationListRes> notificationListResArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(NotificationsActivity.this, LinearLayoutManager.VERTICAL, false);
        rvPackages.setLayoutManager(layoutManager);

        NotificationListRes notificationListRes = PrefHelper.getNotificationListResponse(Constants.NOTIFICATION_LIST);
        if (notificationListRes != null) {
            notificationListResArrayList.add(notificationListRes);
        }

        if (notificationListResArrayList.size() > 0) {
            rvPackages.setVisibility(View.VISIBLE);
            tvNoPackages.setVisibility(View.GONE);
            bindPackagesList();
        } else {
            rvPackages.setVisibility(View.GONE);
            tvNoPackages.setVisibility(View.VISIBLE);
        }
    }

    private void bindPackagesList() {
        if (notificationListResArrayList != null && notificationListResArrayList.size() > 0) {
            rvPackages.setVisibility(View.VISIBLE);
            NotificationListAdapter notificationListAdapter = (NotificationListAdapter) rvPackages.getAdapter();
            if (notificationListAdapter != null && notificationListAdapter.getItemCount() > 0)
                notificationListAdapter.setListItem(notificationListResArrayList);
            else {
                notificationListAdapter = new NotificationListAdapter(NotificationsActivity.this, notificationListResArrayList, this);
                rvPackages.setAdapter(notificationListAdapter);
            }
        }
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void onItemClick(ArrayList<NotificationListRes> item) {
        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
    }
}
