package com.example.distributorapp.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatCheckBox;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.TripPlannerReq;
import com.example.distributorapp.models.TripPlannerRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.example.distributorapp.utils.ToastHelper;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class TripPlannerMenuActivity extends BaseActivity {

    @BindView(R.id.edtArea)
    EditText edtArea;
    @BindView(R.id.edtETD)
    EditText edtETD;
    @BindView(R.id.edtETA)
    EditText edtETA;
    @BindView(R.id.edtPartyNumber)
    EditText edtPartyNumber;
    @BindView(R.id.edtPartyMember)
    EditText edtPartyMember;
    @BindView(R.id.edtExistingMedicalComdition)
    EditText edtExistingMedicalComdition;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.ckhRemember)
    AppCompatCheckBox ckhRemember;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    private String strPartyMember = "", strETADate = "", strETATime = "", strETDDate = "", strETDTime = "";

    private int mYear, mMonth, mDay, mYear1, mMonth1, mDay1,mHour, mMinute;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_planner_menu);
        ButterKnife.bind(this);
        getAndDisplayDeatils();

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        mYear1 = c.get(Calendar.YEAR);
        mMonth1 = c.get(Calendar.MONTH);
        mDay1 = c.get(Calendar.DAY_OF_MONTH);
    }

    public void getAndDisplayDeatils() {
        edtArea.setText(PrefHelper.getString(Constants.AREA, ""));
        edtETD.setText(PrefHelper.getString(Constants.ETD, ""));
        edtETA.setText(PrefHelper.getString(Constants.ETA, ""));
        edtPartyMember.setText(PrefHelper.getString(Constants.PARTY_MEMBER, ""));
        edtPartyNumber.setText(PrefHelper.getString(Constants.PARTY_NUMBER, ""));
        edtExistingMedicalComdition.setText(PrefHelper.getString(Constants.MEDICAL_CONDITION, ""));
    }

    @OnClick({R.id.btnRegister, R.id.ivBack, R.id.edtETA, R.id.edtETD, R.id.edtPartyMember, R.id.btnSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (isValidate()) {
                    callTripPlannerAPI();
                }
                break;

            case R.id.ivBack:
                finish();
                break;

            case R.id.edtETA:
                displayDatePickerETADialog();
                break;

            case R.id.edtETD:
                displayDatePickerETDDialog();
                break;

            case R.id.edtPartyMember:
                displayNumberPad();
                break;

            case R.id.btnSend:
                postTripPlannerDetails();
                break;
        }
    }

    public boolean isValidate() {
        boolean isValid = true;
        String strArea = edtArea.getText().toString().trim();
        String strETA = edtETA.getText().toString().trim();
        String strETD = edtETD.getText().toString().trim();
        String strPartyNumber = edtPartyNumber.getText().toString().trim();
        strPartyMember = edtPartyMember.getText().toString().trim();
        String strExistingMedicalComdition = edtExistingMedicalComdition.getText().toString().trim();
        if (strArea.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter General Area of Information", Toast.LENGTH_SHORT).show();
        } else if (strETD.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter ETD", Toast.LENGTH_SHORT).show();
        } else if (strETA.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter ETA", Toast.LENGTH_SHORT).show();
        } else if (strPartyNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Party Number", Toast.LENGTH_SHORT).show();
        } else if (strPartyMember.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Party Member", Toast.LENGTH_SHORT).show();
        } else if (strExistingMedicalComdition.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Existing Medical Condition", Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }

    private void callTripPlannerAPI() {
        TripPlannerReq tripPlannerReq = new TripPlannerReq();
        tripPlannerReq.setEta(edtETA.getText().toString().trim());
        tripPlannerReq.setEtd(edtETD.getText().toString().trim());
        tripPlannerReq.setExisting_medical_condition(edtExistingMedicalComdition.getText().toString().trim());
        tripPlannerReq.setGeneral_area_adventure(edtArea.getText().toString().trim());
        tripPlannerReq.setParty_members(edtPartyNumber.getText().toString().trim());
        tripPlannerReq.setParty_name(edtPartyMember.getText().toString().trim());
        tripPlannerReq.setUser_type("1");

        Call<TripPlannerRes> objectCall = RestClient.getApiClient().tripPlanner(tripPlannerReq);
        restClient.makeApiRequest(TripPlannerMenuActivity.this, objectCall, this, Constants.REQ_CODE_ADD_TRIP_PLANNER, true);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_ADD_TRIP_PLANNER:
                TripPlannerRes loginRes = (TripPlannerRes) response.body();
                if (loginRes != null) {
                    if (loginRes.isSuccess()) {
                        PrefHelper.setBoolean(Constants.IS_ADDED_TRIP_PLANNER, true);
                        PrefHelper.setString(Constants.AREA, edtArea.getText().toString().trim());
                        PrefHelper.setString(Constants.ETD, edtETD.getText().toString().trim());
                        PrefHelper.setString(Constants.ETA, edtETA.getText().toString().trim());
                        PrefHelper.setString(Constants.PARTY_MEMBER, edtPartyMember.getText().toString().trim());
                        PrefHelper.setString(Constants.PARTY_NUMBER, edtPartyNumber.getText().toString().trim());
                        PrefHelper.setString(Constants.MEDICAL_CONDITION, edtExistingMedicalComdition.getText().toString().trim());
                        //Toast.makeText(TripPlannerMenuActivity.this, "Update successfull", Toast.LENGTH_SHORT).show();
                        ToastHelper.getInstance().showToast(TripPlannerMenuActivity.this, "Update successfull", Toast.LENGTH_SHORT);
                        /*new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(TripPlannerMenuActivity.this, MainActivity.class));
                                finish();
                            }
                        }, 1800);*/
                    } else {
                        Toast.makeText(TripPlannerMenuActivity.this, "" + loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(TripPlannerMenuActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_ADD_TRIP_PLANNER:
                Toast.makeText(TripPlannerMenuActivity.this, object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(TripPlannerMenuActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void displayDatePickerETDDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;

                        strETDDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        //edtETD.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        displayETDTimePickerDialog();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    public void displayDatePickerETADialog() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mYear1 = year;
                        mMonth1 = monthOfYear;
                        mDay1 = dayOfMonth;

                        strETADate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        //edtETA.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        displayETATimePickerDialog();
                    }
                }, mYear1, mMonth1, mDay1);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    public void displayETATimePickerDialog() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(TripPlannerMenuActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        strETATime = hourOfDay +" : " +minute;
                        edtETA.setText(strETADate +" "+ strETATime);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void displayETDTimePickerDialog() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(TripPlannerMenuActivity.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        strETDTime = hourOfDay +" : " +minute;
                        edtETD.setText(strETDDate +" "+ strETDTime);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void displayNumberPad() {
        final CharSequence[] choice = new CharSequence[200];

        for (int i=0; i<200; i++) {
            choice[i] = ""+(i+1);
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(TripPlannerMenuActivity.this);
        alert.setTitle("Select party members");
        alert.setSingleChoiceItems(choice, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                strPartyMember = ""+choice[which];
                edtPartyMember.setText(strPartyMember);
                dialog.dismiss();
            }
        });

        alert.show();
    }

    public void postTripPlannerDetails() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("message/rfc822");

        //intent.putExtra(Intent.EXTRA_SUBJECT, "Planner Details");
        //intent.putExtra(Intent.EXTRA_TEXT , ""+addDetails());
        intent.setData(Uri.parse("mailto:"+PrefHelper.getString(Constants.EMAIL, ""))); // or just "mailto:" for blank

        String uriText = "mailto:" + Uri.encode(PrefHelper.getString(Constants.EMAIL, "")) +
                "?subject=" + Uri.encode("Planner Details") +
                "&body=" + Uri.encode(addDetails());
        Uri uri = Uri.parse(uriText);

        intent.setData(uri);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(intent);
            //startActivity(Intent.createChooser(intent, "Share data!"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(TripPlannerMenuActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    /*public String addDetails() {
        String strDetails = "General Area of Information : "+Uri.encode(PrefHelper.getString(Constants.AREA, ""))+"\n"+
                "ETD : "+Uri.encode(PrefHelper.getString(Constants.ETD, ""))+"\n"+
                "ETA : "+Uri.encode(PrefHelper.getString(Constants.ETA, ""))+"\n"+
                "Party Number : "+Uri.encode(PrefHelper.getString(Constants.PARTY_NUMBER, ""))+"\n"+
                "Party Member : "+Uri.encode(PrefHelper.getString(Constants.PARTY_MEMBER, ""))+"\n"+
                "Existing Medical Condition : "+Uri.encode(PrefHelper.getString(Constants.MEDICAL_CONDITION, ""));
        return strDetails;
    }*/

    public String addDetails() {
        String strDetails = "General Area of Information : "+PrefHelper.getString(Constants.AREA, "")+"\n"+
                "ETD : "+PrefHelper.getString(Constants.ETD, "")+"\n"+
                "ETA : "+PrefHelper.getString(Constants.ETA, "")+"\n"+
                "Party Names : "+PrefHelper.getString(Constants.PARTY_NUMBER, "")+"\n"+
                "Party Member : "+PrefHelper.getString(Constants.PARTY_MEMBER, "")+"\n"+
                "Existing Medical Condition : "+PrefHelper.getString(Constants.MEDICAL_CONDITION, "");
        return strDetails;
    }
}
