package com.example.distributorapp.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.example.distributorapp.R;
import com.example.distributorapp.fragments.DashFragment;
import com.example.distributorapp.fragments.PaymentFragment;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PermissionUtils;
import com.example.distributorapp.utils.PrefHelper;
import com.example.distributorapp.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImmidiateAssistanceActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
        PermissionUtils.PermissionResultCallback {

    /*@BindView(R.id.ivBack)
    ImageView ivBack;*/
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.btnUserSetup)
    Button btnUserSetup;
    @BindView(R.id.ivSwipe)
    ImageView ivSwipe;
    @BindView(R.id.tvCount)
    TextView tvCount;
    @BindView(R.id.timer)
    LottieAnimationView timer;
    @BindView(R.id.sending)
    LottieAnimationView sending;
    @BindView(R.id.llSent)
    LinearLayout llSent;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.btnStop)
    Button btnStop;

    private int count = 0, highCount = 5;

    private boolean isStop = false;

    private android.app.AlertDialog.Builder alert;
    private android.app.AlertDialog alertDialog;

    private boolean isPermissionGranted = false;
    private ArrayList<String> permissions = new ArrayList<>();
    private PermissionUtils permissionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistance);
        ButterKnife.bind(this);

        permissionUtils = new PermissionUtils(ImmidiateAssistanceActivity.this);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissions.add(Manifest.permission.SEND_SMS);
        permissionUtils.check_permission(permissions, "Need  permission", 1);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(GuidelineActivity.this, SOSService.class));
        } else {
            startService(new Intent(GuidelineActivity.this, SOSService.class));
        }*/

        //startService(new Intent(GuidelineActivity.this, LockService.class));
        /*Intent backgroundService = new Intent(getApplicationContext(), ScreenOnOffBackgroundService.class);
        startService(backgroundService);*/
    }

    @OnClick({R.id.btnSignIn, R.id.btnUserSetup, R.id.ivSwipe, R.id.ivBack, R.id.btnStop})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:
                startActivity(new Intent(ImmidiateAssistanceActivity.this, SignInActivity.class));
                finish();
                break;
            case R.id.btnUserSetup:
                startActivity(new Intent(ImmidiateAssistanceActivity.this, SignUpActivity.class));
                finish();
                break;
            case R.id.ivSwipe:
                //swipeAndSendMessage();
                if (ActivityCompat.checkSelfPermission(ImmidiateAssistanceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ImmidiateAssistanceActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();
                } else {
                    /*if (!PrefHelper.getBoolean(Constants.IS_CONFORM_SMS, false)) {
                        dialogSMSAlert();
                    } else {*/
                        if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                            count = 1;
                            ivSwipe.setVisibility(View.GONE);
                            tvCount.setVisibility(View.VISIBLE);
                            btnStop.setVisibility(View.VISIBLE);
                            tvCount.setText("" + highCount);
                            RunAnimation();
                            animateCount();
                            isStop = false;
                            //sendMessageAnimation();
                        } else {
                            PaymentFragment bottomSheetFragment = new PaymentFragment();
                            bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                        }
                    //}

                }
                break;
            case R.id.ivBack:
                finish();
                break;

            case R.id.btnStop:
                isStop = true;
                ivSwipe.setVisibility(View.VISIBLE);
                tvCount.setVisibility(View.GONE);
                btnStop.setVisibility(View.GONE);
                tvCount.clearAnimation();
                highCount = 5;

                break;
        }
    }

    private void dialogSMSAlert() {
        LayoutInflater li = LayoutInflater.from(ImmidiateAssistanceActivity.this);
        final View confirmDialog = li.inflate(R.layout.dialog_sms_alert, null);

        Button btnConfirm = (Button)confirmDialog.findViewById(R.id.btnConfirm);
        Button btnCancel = (Button)confirmDialog.findViewById(R.id.btnCancel);

        alert = new android.app.AlertDialog.Builder(ImmidiateAssistanceActivity.this);
        alert.setView(confirmDialog);
        alert.setCancelable(true);
        alertDialog = alert.create();
        alertDialog.show();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                PrefHelper.setBoolean(Constants.IS_CONFORM_SMS, true);
                if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                    count = 1;
                    ivSwipe.setVisibility(View.GONE);
                    tvCount.setVisibility(View.VISIBLE);
                    btnStop.setVisibility(View.VISIBLE);
                    tvCount.setText("" + highCount);
                    RunAnimation();
                    animateCount();
                    isStop = false;
                    //sendMessageAnimation();
                } else {
                    PaymentFragment bottomSheetFragment = new PaymentFragment();
                    bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void animateCount() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isStop) {
                    if (highCount != 0) {
                        highCount = highCount - count;
                        tvCount.setText("" + highCount);
                        animateCount();
                        RunAnimation();
                    } else if (highCount == 0) {
                        if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                            if (Utils.getInstance().checkInternetConnection(ImmidiateAssistanceActivity.this)) {
                                if (PrefHelper.getBoolean(Constants.IS_LOGIN, false)) {
                                    //if (PrefHelper.getBoolean(Constants.IS_SEND_MESSAGE, false)) {
                                    String phoneNo = PrefHelper.getString(Constants.PRIMARY_EMERGENCY_NUMBER, "");
                                    if (phoneNo.startsWith("0")) {
                                        phoneNo = phoneNo.substring(1);
                                    }
                                    String senderNumber = PrefHelper.getString(Constants.COUNTRY_CODE, "") + phoneNo;
                                    if (DashFragment.currentLatitude != 0.0 && DashFragment.currentLongitude != 0.0) {
                                        //sendSMS("" + senderNumber, "IMMEDIATE ASSISTANCE REQUIRED : Address - " + DashFragment.strAddress + ", Lat : " + String.format("%.4f", DashFragment.currentLatitude) + "  Long : " + String.format("%.4f", DashFragment.currentLongitude) + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                                        sendSMS("" + senderNumber, "IMMEDIATE ASSISTANCE REQUIRED : Address - " + DashFragment.strAddress + ", http://maps.google.com/?q=" + String.format("%.4f", DashFragment.currentLatitude) + "," + String.format("%.4f", DashFragment.currentLongitude) + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                                    } else {
                                        //sendSMS("" + senderNumber, "IMMEDIATE ASSISTANCE REQUIRED : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                                        sendSMS("" + senderNumber, "IMMEDIATE ASSISTANCE REQUIRED : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                                    }


                                }
                            } else {
                                Toast.makeText(ImmidiateAssistanceActivity.this, "Network is not avaialable so once network is available then it will be sent message automatically", Toast.LENGTH_LONG).show();
                                String message = "";
                                if (DashFragment.currentLatitude != 0.0 && DashFragment.currentLongitude != 0.0) {
                                    message = "IMMEDIATE ASSISTANCE REQUIRED : Address - " + DashFragment.strAddress + ", Lat : " + String.format("%.4f", DashFragment.currentLatitude) + "  Long : " + String.format("%.4f", DashFragment.currentLongitude) + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, "");
                                } else {
                                    message = "IMMEDIATE ASSISTANCE REQUIRED : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, "");
                                }

                                PrefHelper.setString(Constants.STORED_MESSAGE, "" + message);
                            }
                        } else {
                            PaymentFragment bottomSheetFragment = new PaymentFragment();
                            bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                        }
                        llSent.setVisibility(View.VISIBLE);
                        tvCount.setVisibility(View.GONE);
                        ivSwipe.setVisibility(View.VISIBLE);
                        btnStop.setVisibility(View.GONE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                llSent.setVisibility(View.GONE);
                                highCount = 5;
                            }
                        }, 3000);
                    }
                }
            }
        }, 800);
    }

    private void RunAnimation() {
        Animation a = AnimationUtils.loadAnimation(this, R.anim.scale);
        a.reset();
        tvCount.clearAnimation();
        tvCount.startAnimation(a);
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            //msg = "http://maps.google.com/?q=23.0225,72.5818";
            //msg = "geo:0,0?q=23.0225,72.5818";
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            ArrayList<String> parts = smsManager.divideMessage(msg);
            smsManager.sendMultipartTextMessage(phoneNo, null, parts, null, null);

            /*Uri uri = Uri.parse("smsto:"+phoneNo);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", msg);
            startActivity(intent);*/
        } catch (Exception ex) {
            Toast.makeText(ImmidiateAssistanceActivity.this, ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION", "GRANTED");
        isPermissionGranted = true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY", "GRANTED");
//        isPermissionGranted = true;
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION", "DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION", "NEVER ASK AGAIN");
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }
}
