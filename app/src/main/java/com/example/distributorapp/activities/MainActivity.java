package com.example.distributorapp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.example.distributorapp.R;
import com.example.distributorapp.fragments.Payment1Fragment;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends BaseActivity implements
                                    NavigationView.OnNavigationItemSelectedListener {
    public Toolbar toolbar;
    public DrawerLayout drawerLayout;
    public NavController navController;
    public NavigationView navigationView;

    private static final String SHARE_SUBJECT = "Personal Locator Relay";
    private static final String SHARE_MESSAGE = "Place your Emergency SOS Alert app on your home screen for quick access, giving you the ability to alert your SOS contacts in seconds." +
            "\n\nDownload from here:\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupNavigation();

    }

    // Setting Up One Time Navigation
    private void setupNavigation() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu));
        toolbar.setTitleTextAppearance(this, R.style.ToolbarTextAppearance);

        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.navigationView);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout);

        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);

        /*ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout, toolbar,R.string.app_name, R.string.app_name);
        mDrawerToggle.getDrawerArrowDrawable().setColor(Color.parseColor("#006e65"));
        drawerLayout.addDrawerListener(mDrawerToggle);*/
        //mDrawerToggle.syncState();

        //navigationView.getHeaderView(0).findViewById(R.id.tvUserName).setTag("Ideal");
        View headerView = navigationView.getHeaderView(0);
        TextView tvUserName = (TextView) headerView.findViewById(R.id.tvUserName);
        tvUserName.setText(""+PrefHelper.getString(Constants.FULL_NAME, ""));

        TextView tvEmail = (TextView) headerView.findViewById(R.id.tvEmail);
        tvEmail.setText(""+PrefHelper.getString(Constants.EMAIL, ""));

        TextView tvVersion = (TextView) headerView.findViewById(R.id.tvVersion);
        tvVersion.setText("Version "+getVersionName());
    }

    public String getVersionName() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(drawerLayout, Navigation.findNavController(this, R.id.nav_host_fragment));
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            showExitDialog();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        menuItem.setChecked(true);

        drawerLayout.closeDrawers();

        int id = menuItem.getItemId();

        switch (id) {

            case R.id.menu_dashboard:
                //navController.navigate(R.id.firstFragment);
                break;

            case R.id.menu_asistance:
                //navController.navigate(R.id.firstFragment);
                startActivity(new Intent(MainActivity.this, ImmidiateAssistanceActivity.class));
                break;

            case R.id.menu_trip_planner:
                //navController.navigate(R.id.firstFragment);
                startActivity(new Intent(MainActivity.this, TripPlannerMenuActivity.class));
                break;

            case R.id.menu_subscription:
                //navController.navigate(R.id.firstFragment);
                startActivity(new Intent(MainActivity.this, PackageListActivity.class));
                break;

            case R.id.menu_membership:
                //navController.navigate(R.id.firstFragment);
                startActivity(new Intent(MainActivity.this, ActivatedPackagesActivity.class));
                break;

            case R.id.menu_my_profile:
                //navController.navigate(R.id.firstFragment);
                startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
                break;

            case R.id.menu_notification:
                if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                    startActivity(new Intent(MainActivity.this, NotificationsActivity.class));
                } else {
                    Payment1Fragment bottomSheetFragment = new Payment1Fragment();
                    bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                }
                break;

            case R.id.menu_feedback:
                postFeedback();
                break;

            case R.id.menu_share:
                Intent share = createShareIntent();
                startActivity(Intent.createChooser(share, "Share App"));
                break;

            case R.id.menu_privacy_policy:
                String url = "https://plrsafety.co.nz/privacy-policy/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;

            case R.id.menu_contact_us:
                String url1 = "https://plrsafety.co.nz/contact-us/";
                Intent i1 = new Intent(Intent.ACTION_VIEW);
                i1.setData(Uri.parse(url1));
                startActivity(i1);
                break;

            case R.id.menu_logout:
                showLogoutDialog();
                break;


            /*case R.id.second:
                navController.navigate(R.id.secondFragment);
                break;

            case R.id.third:
                navController.navigate(R.id.thirdFragment);
                break;*/

        }
        return true;

    }

    public void postFeedback() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("message/rfc822");
        //i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"recipient@example.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Feedback of PLR APP");
        i.putExtra(Intent.EXTRA_TEXT   , "");
        i.setData(Uri.parse("mailto:hello@binarycode.co.nz")); // or just "mailto:" for blank
        try {
            startActivity(i);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, SHARE_SUBJECT);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "" + SHARE_MESSAGE + "https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName()/*Html.fromHtml(SHARE_MESSAGE+"<a>https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()+"</a>")*/);
        return shareIntent;
    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure to want to exit from app?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    private void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure to want to logout from app?");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PrefHelper.setBoolean(Constants.IS_LOGIN, false);
                        PrefHelper.setString(Constants.ACCESS_TOKEN, "");
                        PrefHelper.setString(Constants.PRIMARY_EMERGENCY_NUMBER, "");
                        PrefHelper.setString(Constants.SECONDARY_EMERGENCY_NUMBER, "");
                        PrefHelper.setString(Constants.FULL_NAME, "");
                        PrefHelper.setString(Constants.PASSWORD, "");
                        PrefHelper.setString(Constants.PERSONAL_MOBILE_NUMBER, "");
                        PrefHelper.setString(Constants.COUNTRY_CODE, "");
                        PrefHelper.setString(Constants.EMAIL, "");
                        PrefHelper.setBoolean(Constants.IS_PAID_USER, false);
                        startActivity(new Intent(MainActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                });

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }
}
