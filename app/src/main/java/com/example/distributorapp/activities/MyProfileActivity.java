package com.example.distributorapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatCheckBox;

import com.example.distributorapp.R;
import com.example.distributorapp.apis.RestClient;
import com.example.distributorapp.models.UpdateProfileReq;
import com.example.distributorapp.models.UpdateProfileRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class MyProfileActivity extends BaseActivity {

    @BindView(R.id.edtLocalNumber)
    EditText edtLocalNumber;
    @BindView(R.id.edtSecondaryNumber)
    EditText edtSecondaryNumber;
    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtPersonalNumber)
    EditText edtPersonalNumber;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtConfirmPassword)
    EditText edtConfirmPassword;
    @BindView(R.id.btnUpdate)
    Button btnUpdate;
    @BindView(R.id.ckhRemember)
    AppCompatCheckBox ckhRemember;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.edtCountryCode)
    EditText edtCountryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        presetData();
    }

    public void presetData() {
        edtFirstName.setText(PrefHelper.getString(Constants.FULL_NAME, ""));
        edtCountryCode.setText(""+PrefHelper.getString(Constants.COUNTRY_CODE, ""));
        edtLocalNumber.setText(PrefHelper.getString(Constants.PRIMARY_EMERGENCY_NUMBER, ""));
        edtSecondaryNumber.setText(PrefHelper.getString(Constants.SECONDARY_EMERGENCY_NUMBER, ""));
        edtPersonalNumber.setText(PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
        edtPassword.setText(PrefHelper.getString(Constants.PASSWORD, ""));
        edtConfirmPassword.setText(PrefHelper.getString(Constants.PASSWORD, ""));
    }

    @OnClick({R.id.btnUpdate, R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (isValidate()) {
                    callSignUpAPI();
                }
                break;

            case R.id.ivBack:
                finish();
                break;

        }
    }

    public boolean isValidate() {
        boolean isValid = true;
        String strPrimaryEmergencyNumber = edtLocalNumber.getText().toString().trim();
        String strSecondaryEmergencyNumber = edtSecondaryNumber.getText().toString().trim();
        String strName = edtFirstName.getText().toString().trim();
        String strMobileNumber = edtPersonalNumber.getText().toString().trim();
        //String strEmail = edtEmailAddress.getText().toString().trim();
        String strPassword = edtPassword.getText().toString().trim();
        String strConfirmPassword = edtConfirmPassword.getText().toString().trim();
        if (strPrimaryEmergencyNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter local emergency contact", Toast.LENGTH_SHORT).show();
        } /*else if (strSecondaryEmergencyNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter secondary emergency number", Toast.LENGTH_SHORT).show();
        }*/ else if (strName.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Name", Toast.LENGTH_SHORT).show();
        } else if (strMobileNumber.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter personal Mobile Number", Toast.LENGTH_SHORT).show();
        } /*else if (strEmail.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
        } else if (!Utils.isValidEmaillId(strEmail)) {
            isValid = false;
            Toast.makeText(this, "Please enter valid Email", Toast.LENGTH_SHORT).show();
        } */ else if (strPassword.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Password", Toast.LENGTH_SHORT).show();
        } else if (strConfirmPassword.length() == 0) {
            isValid = false;
            Toast.makeText(this, "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
        } else if (!strPassword.equalsIgnoreCase(strConfirmPassword)) {
            isValid = false;
            Toast.makeText(this, "Password and Confirm Password must be same", Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }

    private void callSignUpAPI() {
        UpdateProfileReq signUpReq = new UpdateProfileReq();
        signUpReq.setPassword(edtPassword.getText().toString().trim());
        signUpReq.setEmergency_primary_contact_number(edtLocalNumber.getText().toString().trim());
        signUpReq.setEmergency_secondary_contact_number(edtSecondaryNumber.getText().toString().trim());
        signUpReq.setName(edtFirstName.getText().toString().trim());
        signUpReq.setPassword_confirmation(edtConfirmPassword.getText().toString().trim());
        signUpReq.setPhone_number(edtPersonalNumber.getText().toString().trim());

        Call<UpdateProfileRes> objectCall = RestClient.getApiClient().updateProfile(signUpReq);
        restClient.makeApiRequest(MyProfileActivity.this, objectCall, this, Constants.REQ_CODE_UPDATE_PROFILE, true);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_UPDATE_PROFILE:
                UpdateProfileRes loginRes = (UpdateProfileRes) response.body();
                if (loginRes != null) {
                    if (loginRes.isSuccess()) {
                        PrefHelper.setString(Constants.PRIMARY_EMERGENCY_NUMBER, edtLocalNumber.getText().toString().trim());
                        PrefHelper.setString(Constants.SECONDARY_EMERGENCY_NUMBER, edtSecondaryNumber.getText().toString().trim());
                        PrefHelper.setString(Constants.FULL_NAME, edtFirstName.getText().toString().trim());
                        PrefHelper.setString(Constants.PERSONAL_MOBILE_NUMBER, edtPersonalNumber.getText().toString().trim());
                        //PrefHelper.setString(Constants.EMAIL, edtEmailAddress.getText().toString().trim());
                        Toast.makeText(MyProfileActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(MyProfileActivity.this, "" + loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MyProfileActivity.this, loginRes.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_UPDATE_PROFILE:
                Toast.makeText(MyProfileActivity.this, object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(MyProfileActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
