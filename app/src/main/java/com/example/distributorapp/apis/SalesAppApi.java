package com.example.distributorapp.apis;

import com.example.distributorapp.models.CountryListRes;
import com.example.distributorapp.models.GetUserStateRes;
import com.example.distributorapp.models.LoginReq;
import com.example.distributorapp.models.LoginRes;
import com.example.distributorapp.models.PackageListRes;
import com.example.distributorapp.models.PostPaymentDataReq;
import com.example.distributorapp.models.PostPaymentDataRes;
import com.example.distributorapp.models.SignUpReq;
import com.example.distributorapp.models.SignUpRes;
import com.example.distributorapp.models.TripPlannerReq;
import com.example.distributorapp.models.TripPlannerRes;
import com.example.distributorapp.models.UpdateProfileReq;
import com.example.distributorapp.models.UpdateProfileRes;
import com.example.distributorapp.models.ValidatePromoCodeReq;
import com.example.distributorapp.models.ValidatePromoCodeRes;
import com.example.distributorapp.models.ValidatePromoCodeRes2;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SalesAppApi {
    /*@POST("user.svc/EmployeeWiseReportForApp")
    Call<PackagesListRes> getCehckInOutDetailsList(@Body PackagesListReq packagesListReq);

    @POST("user.svc/GetDayWiseExpensebyMonth")
    Call<GetExpensesListRes> getExpensesDetailsList(@Body GetExpensesListReq packagesListReq);

    @POST("user.svc/GetExpensebyDate")
    Call<GetExpensesDetailsListRes> getExpensesFullDetailsList(@Body GetExpensesDetailsListReq packagesListReq);

    @POST("dar.svc/GetExistingPotentialAmount")
    Call<PotentialResponse> potential(@Body PotentialReq potentialReq);

    @POST("user.svc/GetMessage")
    Call<NotificationsListRes> getNoticationList(@Body NotificationsListReq notificationsListReq);

    @POST("UserMaster/AddDistributor")
    Call<PostDistributorDataLTBRes> postDistributorDataLTB(@Body PostDistributorDataLTBReq notificationsListReq);

    @POST("user.svc/GetClientDetail")
    Call<ViewClientListRes> getViewClient(@Body ViewClientListReq viewClientListReq);

    @POST("UserMaster/AddCreditToClient")
    Call<AddCreditRes> addCredit(@Body AddCreditReq addCreditReq);

    @POST("user.svc/GetDashBoardData")
    Call<GetDashDataListRes> getDashDataList(@Body GetDashDataListReq getDashDataListReq);

    @POST("OMS.svc/GetProductByHead")
    Call<GetProductListRes> getProductList(@Body GetProductListReq getProductListReq);

    @POST("dar.svc/stockReturn")
    Call<PostStockReturnRes> postStockReturn(@Body PostStockReturnReq postStockReturnReq);

    @POST("user.svc/AddClientSingle")
    Call<PostClientDataRes> postClientDataToServer(@Body PostClientDataReq postClientDataReq);

    @POST("user.svc/EditClient")
    Call<PostClientDataRes> updateClientDataToServer(@Body PostClientDataReq postClientDataReq);

    @POST("user.svc/GetAllSchool")
    Call<GetSchoolListRes> getSchoolList();*/

    /*@POST("IClassUsers/GetUserDetails")
    Call<MyCoursesListRes> getMyCoursesList(@Body MyCoursesListReq myCoursesListReq);

    /*@GET("Values/GetAllMessage?MessageType=3")
    Call<PackagesListRes> getMessagesList();*/

    @POST("api/login")
    Call<LoginRes> login(@Body LoginReq loginReq);

    @POST("api/register")
    Call<SignUpRes> register(@Body SignUpReq signUpReq);

    @POST("api/trip_planner")
    Call<TripPlannerRes> tripPlanner(@Body TripPlannerReq signUpReq);

    @POST("api/user/update")
    Call<UpdateProfileRes> updateProfile(@Body UpdateProfileReq signUpReq);

    @POST("api/check/code")
    Call<ValidatePromoCodeRes> validatePromoCode(@Body ValidatePromoCodeReq validatePromoCodeReq);

    @POST("api/validate/code")
    Call<ValidatePromoCodeRes2> checkAndCreatePromoCode(@Body ValidatePromoCodeReq validatePromoCodeReq);

    @POST("api/user/package")
    Call<PostPaymentDataRes> postPaymentDetails(@Body PostPaymentDataReq postPaymentDataReq);

    @GET("api/countries")
    Call<CountryListRes> getCountryList();

    @GET("api/user/state")
    Call<GetUserStateRes> getUserState();

    @GET("api/packages")
    Call<PackageListRes> getPackageList();
}