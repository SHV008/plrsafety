package com.example.distributorapp.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.distributorapp.R;


public class ToastHelper {
    private static Toast toast = null;
    private static ToastHelper toastHelper;

    public static ToastHelper getInstance() {
        if (toastHelper == null)
            toastHelper = new ToastHelper();
        return toastHelper;
    }


    public void showToast(@NonNull Context context, String message) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showToast(@NonNull Context context, String message, int duration) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, duration);

        Toast toast = Toast.makeText(context, message, duration);
        View view = toast.getView();
        view.getBackground().setColorFilter(context.getResources().getColor(R.color.green), PorterDuff.Mode.SRC_IN);
        TextView text = view.findViewById(android.R.id.message);
        text.setTextColor(context.getResources().getColor(R.color.white));

        toast.show();
    }


    /*public void showToast(@NonNull Context context, String message, int duration, int gravity) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, duration);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }*/
}