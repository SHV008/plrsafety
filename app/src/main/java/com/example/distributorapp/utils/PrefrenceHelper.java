package com.example.distributorapp.utils;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Apple on 12/24/16.
 */
public class PrefrenceHelper {

    public static final String ISUPDATE = "ISUPDATE";
    public static final String ISUPDATE_TEMP = "ISUPDATE_TEMP";
    public static final String USER_ID = "UserId";
    public static final String USER_NAME = "UserName";
    public static final String USER_IMAGE = "UserImage";
    public static final String OUTCOME_ID = "OutComeId";
    public static final String POTENTIAL_ID = "PotentialId";
    public static final String OWN_CAR_RATE = "OWN_CAR_RATE";
    public static final String OWN_TWO_WHEELER_RATE = "OWN_TWO_WHEELER_RATE";
    public static final String DA_LOCAL = "DALocal";
    public static final String DA_OUT_SIDE = "DAOutSide";
    public static final String DA_OUT_SIDE_WITH_NS_200 = "DA Outside (>200 KM)";
    public static final String DA_OUT_SIDE_WITH_NS_300 = "DA Outside (>300 KM)";
    //public static final String DA_OUT_SIDE_WITH_NS = "DAOutSideWithNS";
    public static final String BillingName = "BillingName";
    public static final String isREMEBERME = "isREMEBERME";
    public static final String isFirstTime = "isFirstTime";
    public static final String DEVICE_TOKEN = "DeviceToken";
    public static final String ISSCHOOLUPDATED = "isschoolupdated";


    public static void addUpdateApk(Context context, boolean isUpdate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(ISUPDATE, isUpdate).commit();
    }

    public static void setFirstime(Context context, boolean isRememberMe) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isFirstTime, isRememberMe).commit();
    }

    public static boolean isFirstTIime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isFirstTime, false);
    }

    public static void setDeviceToken(Context context, String deviceToken) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(DEVICE_TOKEN, deviceToken).commit();
    }

    public static String getDeviceToken(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(DEVICE_TOKEN, "");
    }

    public static void setStockBillingName(Context context, String StockBillingName) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(BillingName, StockBillingName).commit();
    }

    public static String getStockBillingName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(BillingName, "");
    }


    public static void setRemeberMe(Context context, boolean isRememberMe) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isREMEBERME, isRememberMe).commit();
    }

    public static boolean isRemeberMe(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isREMEBERME, false);
    }

    public static void addDownloadId(Context context, long downloadId) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(ISUPDATE_TEMP, downloadId).commit();
    }


    public static long getDownloadId(Context context) {

        return PreferenceManager.getDefaultSharedPreferences(context).getLong(ISUPDATE_TEMP, -1);
    }

    public static void addUserID(Context context, int userid) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(USER_ID, userid).commit();
    }

    public static int getUserID(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(USER_ID, 0);
    }

    public static void addUserName(Context context, String userid) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(USER_NAME, userid).commit();
    }

    public static String getUserName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(USER_NAME, "");
    }

    public static void setUserImage(Context context, String userImage) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(USER_IMAGE, userImage).commit();
    }

    public static String getUserImage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(USER_IMAGE, "");
    }

    public static void addOutComeId(Context context, int outid) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(OUTCOME_ID, outid).commit();
    }

    public static int getOutComeId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(OUTCOME_ID, 0);
    }


    public static void addPotentialId(Context context, int outid) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(POTENTIAL_ID, outid).commit();
    }

    public static int getPotentialId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(POTENTIAL_ID, 0);
    }


    public static void addOwnCarRent(Context context, double rate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(OWN_CAR_RATE, (float) rate).commit();
    }

    public static float getOwnCarRent(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(OWN_CAR_RATE, 0);
    }


    public static void addOwnTwoWheelerRent(Context context, double rate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(OWN_TWO_WHEELER_RATE, (float) rate).commit();
    }

    public static float getOwnTwoWheelerRent(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(OWN_TWO_WHEELER_RATE, 0);
    }

    public static void addDALocal(Context context, double rate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(DA_LOCAL, (float) rate).commit();
    }

    public static float getDALocal(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(DA_LOCAL, 0);
    }


    public static void addDAOutSide(Context context, double rate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(DA_OUT_SIDE, (float) rate).commit();
    }

    public static float getDAOutSide(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(DA_OUT_SIDE, 0);
    }

    public static void addOutSideWithNS(Context context, double rate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(DA_OUT_SIDE_WITH_NS_200, (float) rate).commit();
    }

    public static float getOutSideWithNS(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(DA_OUT_SIDE_WITH_NS_200, 0);
    }

    public static void addOutSideWithNS300(Context context, double rate) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(DA_OUT_SIDE_WITH_NS_300, (float) rate).commit();
    }

    public static float getOutSideWithNS300(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(DA_OUT_SIDE_WITH_NS_300, 0);
    }

    public static boolean getIsSchoolUpdated(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(ISSCHOOLUPDATED, false);
    }

    public static void setIsSchoolUpdated(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(ISSCHOOLUPDATED, value).commit();
    }

}
