package com.example.distributorapp.models;

import com.google.gson.annotations.SerializedName;

public class NotificationListRes extends BaseModel {

    @SerializedName("NotificationID")
    private int NotificationID;
    @SerializedName("NotificationName")
    private String NotificationName;
    @SerializedName("NotificationDescription")
    private String NotificationDescription;
    @SerializedName("Date")
    private String Date;
    @SerializedName("IsRead")
    private boolean IsRead;

    public int getNotificationID() {
        return NotificationID;
    }

    public void setNotificationID(int notificationID) {
        NotificationID = notificationID;
    }

    public String getNotificationName() {
        return NotificationName;
    }

    public void setNotificationName(String notificationName) {
        NotificationName = notificationName;
    }

    public String getNotificationDescription() {
        return NotificationDescription;
    }

    public void setNotificationDescription(String notificationDescription) {
        NotificationDescription = notificationDescription;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }
}
