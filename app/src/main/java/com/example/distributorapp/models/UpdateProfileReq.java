package com.example.distributorapp.models;

public class UpdateProfileReq {

    private String password;
    private String name;
    private String password_confirmation;
    private String emergency_primary_contact_number;
    private String emergency_secondary_contact_number;
    private String phone_number;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getEmergency_primary_contact_number() {
        return emergency_primary_contact_number;
    }

    public void setEmergency_primary_contact_number(String emergency_primary_contact_number) {
        this.emergency_primary_contact_number = emergency_primary_contact_number;
    }

    public String getEmergency_secondary_contact_number() {
        return emergency_secondary_contact_number;
    }

    public void setEmergency_secondary_contact_number(String emergency_secondary_contact_number) {
        this.emergency_secondary_contact_number = emergency_secondary_contact_number;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
