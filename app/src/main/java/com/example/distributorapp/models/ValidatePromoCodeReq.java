package com.example.distributorapp.models;

public class ValidatePromoCodeReq {

    private String promo_code;

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }
}
