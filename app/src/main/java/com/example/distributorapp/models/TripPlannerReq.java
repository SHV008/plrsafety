package com.example.distributorapp.models;

public class TripPlannerReq {

    private String general_area_adventure;
    private String etd;
    private String eta;
    private String party_name;
    private String party_members;
    private String existing_medical_condition;
    private String user_type;

    public String getGeneral_area_adventure() {
        return general_area_adventure;
    }

    public void setGeneral_area_adventure(String general_area_adventure) {
        this.general_area_adventure = general_area_adventure;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getParty_name() {
        return party_name;
    }

    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    public String getParty_members() {
        return party_members;
    }

    public void setParty_members(String party_members) {
        this.party_members = party_members;
    }

    public String getExisting_medical_condition() {
        return existing_medical_condition;
    }

    public void setExisting_medical_condition(String existing_medical_condition) {
        this.existing_medical_condition = existing_medical_condition;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }
}
