package com.example.distributorapp.models;

public class SignUpReq {

    private String email;
    private String password;
    private String name;
    private String password_confirmation;
    private String emergency_primary_contact_number;
    private String emergency_secondary_contact_number;
    private String address;
    private String phone_number;
    private String device_token;
    private String country_id;
    private String promo_code;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getEmergency_primary_contact_number() {
        return emergency_primary_contact_number;
    }

    public void setEmergency_primary_contact_number(String emergency_primary_contact_number) {
        this.emergency_primary_contact_number = emergency_primary_contact_number;
    }

    public String getEmergency_secondary_contact_number() {
        return emergency_secondary_contact_number;
    }

    public void setEmergency_secondary_contact_number(String emergency_secondary_contact_number) {
        this.emergency_secondary_contact_number = emergency_secondary_contact_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }
}
