package com.example.distributorapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRes extends BaseModel {

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data implements Serializable {

        @SerializedName("access_token")
        @Expose
        private String access_token;
        @SerializedName("token_type")
        @Expose
        private String token_type;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("emergency_primary_contact_number")
        @Expose
        private String emergency_primary_contact_number;
        @SerializedName("emergency_secondary_contact_number")
        @Expose
        private String emergency_secondary_contact_number;
        @SerializedName("phone_number")
        @Expose
        private String phone_number;
        @SerializedName("dial_code")
        @Expose
        private String country_code;
        @SerializedName("general_area_adventure")
        @Expose
        private String general_area_adventure;
        @SerializedName("etd")
        @Expose
        private String etd;
        @SerializedName("eta")
        @Expose
        private String eta;
        @SerializedName("party_name")
        @Expose
        private String party_name;
        @SerializedName("party_members")
        @Expose
        private String party_members;
        @SerializedName("existing_medical_condition")
        @Expose
        private String existing_medical_condition;

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmergency_primary_contact_number() {
            return emergency_primary_contact_number;
        }

        public void setEmergency_primary_contact_number(String emergency_primary_contact_number) {
            this.emergency_primary_contact_number = emergency_primary_contact_number;
        }

        public String getEmergency_secondary_contact_number() {
            return emergency_secondary_contact_number;
        }

        public void setEmergency_secondary_contact_number(String emergency_secondary_contact_number) {
            this.emergency_secondary_contact_number = emergency_secondary_contact_number;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getGeneral_area_adventure() {
            return general_area_adventure;
        }

        public void setGeneral_area_adventure(String general_area_adventure) {
            this.general_area_adventure = general_area_adventure;
        }

        public String getEtd() {
            return etd;
        }

        public void setEtd(String etd) {
            this.etd = etd;
        }

        public String getEta() {
            return eta;
        }

        public void setEta(String eta) {
            this.eta = eta;
        }

        public String getParty_name() {
            return party_name;
        }

        public void setParty_name(String party_name) {
            this.party_name = party_name;
        }

        public String getParty_members() {
            return party_members;
        }

        public void setParty_members(String party_members) {
            this.party_members = party_members;
        }

        public String getExisting_medical_condition() {
            return existing_medical_condition;
        }

        public void setExisting_medical_condition(String existing_medical_condition) {
            this.existing_medical_condition = existing_medical_condition;
        }
    }
}
