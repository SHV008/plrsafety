package com.example.distributorapp.models;

public class NotificationListReq {

    private String UserId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}
