package com.example.distributorapp.interfaces;


public interface BackPressedEventListener {
	public void onBackPressed();
}
