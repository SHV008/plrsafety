package com.example.distributorapp.firebase;

import com.example.distributorapp.utils.PrefrenceHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FireIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String tkn = FirebaseInstanceId.getInstance().getToken();
        PrefrenceHelper.setDeviceToken(this, tkn);
    }
}