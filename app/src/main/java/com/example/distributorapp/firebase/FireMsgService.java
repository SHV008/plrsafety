package com.example.distributorapp.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.distributorapp.R;
import com.example.distributorapp.activities.MainActivity;
import com.example.distributorapp.activities.SignInActivity;
import com.example.distributorapp.activities.SplashActivity;
import com.example.distributorapp.models.NotificationListRes;
import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class FireMsgService extends FirebaseMessagingService {

    final int random = new Random().nextInt((1000 - 1) + 1) + 1;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("Msg", "Message received [" + remoteMessage + "]");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int num = (int) System.currentTimeMillis();

        Intent intent = null;
        intent = new Intent(this, SplashActivity.class);
        /*if (remoteMessage.getNotification().getTag().toString().equalsIgnoreCase("1")) {
            if (PrefHelper.getInt(Constants.USER_ID, 0) != 0) {
                intent = new Intent(this, MainActivity.class);
                Intent intent1 = new Intent();
                intent1.setAction("UpdateNotificationCount");
                sendBroadcast(intent1);
            } else {
                intent = new Intent(this, SignInActivity.class);
            }
        } else {
            intent = new Intent(this, SignInActivity.class);
        }*/

        try {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd MMM, yyyy");
            String formattedDate = df.format(c);

            NotificationListRes notificationListRes = new NotificationListRes();
            notificationListRes.setNotificationName(remoteMessage.getNotification().getTitle());
            notificationListRes.setNotificationDescription(remoteMessage.getNotification().getBody());
            notificationListRes.setDate(formattedDate);
            notificationListRes.setRead(false);
            PrefHelper.setNotificationListResponse(Constants.NOTIFICATION_LIST, notificationListRes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create Notification
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, num,
                intent, PendingIntent.FLAG_ONE_SHOT);

        String NOTIFICATION_CHANNEL_ID = "10001";

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new
                NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getNotification().getBody()))
                //.setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "SalesAppChannel", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert notificationManager != null;
            notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(num, notificationBuilder.build());
    }
}
