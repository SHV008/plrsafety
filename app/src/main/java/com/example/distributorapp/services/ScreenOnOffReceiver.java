package com.example.distributorapp.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Imtiyaz K on 12-Sep-19.
 */
public class ScreenOnOffReceiver extends BroadcastReceiver {

    public final static String SCREEN_TOGGLE_TAG = "SCREEN_TOGGLE_TAG";

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Received", Toast.LENGTH_SHORT).show();
        String action = intent.getAction();
        if(Intent.ACTION_SCREEN_OFF.equals(action))
        {
            Log.v(SCREEN_TOGGLE_TAG, "Screen is turn off.");
        }else if(Intent.ACTION_SCREEN_ON.equals(action))
        {
            Log.v(SCREEN_TOGGLE_TAG, "Screen is turn on.");
        }}
}