package com.example.distributorapp.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.PrefHelper;

public class ScreenReceiver extends BroadcastReceiver {

    public static boolean wasScreenOn = true;
    public static int count = 0;

    private Context mContext;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        mContext = context;
        if (PrefHelper.getBoolean(Constants.IS_LOGIN, false)) {
            if (PrefHelper.getBoolean(Constants.IS_SEND_MESSAGE, false)) {
                //Toast.makeText(context, "Toast : " + count, Toast.LENGTH_SHORT).show();
                if (count >= 2) {
                    String senderNumber = "+"+PrefHelper.getString(Constants.COUNTRY_CODE, "")+PrefHelper.getString(Constants.PRIMARY_EMERGENCY_NUMBER, "");
                    if (LockService.currentLatitude != 0.0 && LockService.currentLongitude != 0.0) {
                        sendSMS("" + senderNumber, "PLR : Address - " + LockService.strAddress + ", Lat : " + LockService.currentLatitude + "  Long : " + LockService.currentLongitude + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                    } else {
                        sendSMS("" + senderNumber, "PLR : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                    }
                } else {
                    count = count + 1;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            count = 0;
                        }
                    }, 2000);
                }
            }
        }
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            if (phoneNo.startsWith("0")) {
                phoneNo = phoneNo.substring(1);
            }
            Log.v("Send Message", msg);
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(mContext, "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(mContext,ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
}


        /*if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            // do whatever you need to do here
            wasScreenOn = false;
            Log.v("LOB","wasScreenOn"+wasScreenOn);
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            // and do whatever you need to do here
            wasScreenOn = true;
        } else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
            Log.v("LOB","userpresent");
            Log.v("LOB","wasScreenOn"+wasScreenOn);
            String url = "http://www.stackoverflow.com";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        }*/
