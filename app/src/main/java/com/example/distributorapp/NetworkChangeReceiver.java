package com.example.distributorapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.distributorapp.utils.Constants;
import com.example.distributorapp.utils.NetworkUtil;
import com.example.distributorapp.utils.PrefHelper;

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, final Intent intent) {

        String status = NetworkUtil.getConnectivityStatusString(context);

        //Toast.makeText(context, status, Toast.LENGTH_LONG).show();

        if (!TextUtils.isEmpty(PrefHelper.getString(Constants.STORED_MESSAGE, ""))) {
            String senderNumber = PrefHelper.getString(Constants.COUNTRY_CODE, "") + PrefHelper.getString(Constants.PRIMARY_EMERGENCY_NUMBER, "");
            sendSMS(context, "" + senderNumber, "" + PrefHelper.getString(Constants.STORED_MESSAGE, ""));
            PrefHelper.setString(Constants.STORED_MESSAGE, "");
        }
    }

    public void sendSMS(Context mContext, String phoneNo, String msg) {
        try {
            if (phoneNo.startsWith("0")) {
                phoneNo = phoneNo.substring(1);
            }
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(mContext, "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(mContext, ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
}
