package com.binarycode.plrsafety.interfaces;


public interface BackPressedEventListener {
	public void onBackPressed();
}
