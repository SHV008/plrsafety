package com.binarycode.plrsafety;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.widget.TextView;

import com.binarycode.plrsafety.R;

public class ImmediateAssistanceActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immediate_assistance);

        mTextView = (TextView) findViewById(R.id.text);
    }
}
